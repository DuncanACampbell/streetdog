//
//  OutstandingLessonVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConversationDO;

@interface OutstandingLessonVO : NSObject {
    
    BOOL isLesson;
    NSString *title;
    OutstandingLessonStatusType status;
    
    LessonDO *lesson;
    ConversationDO *conversation;
}

- (id)initWithOutstandingLessonDO:(OutstandingLessonDO*)outstandingLesson;
- (id)initWithConversationDO:(ConversationDO*)nextConversation withStatusType:(OutstandingLessonStatusType)theStatus;

- (BOOL)isLesson;
- (NSString*)title;
- (OutstandingLessonStatusType)status;
- (LessonDO*)lesson;
- (ConversationDO*)conversation;
@end
