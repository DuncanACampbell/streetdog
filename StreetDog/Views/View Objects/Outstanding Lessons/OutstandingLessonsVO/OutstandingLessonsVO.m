//
//  OutstandingLessonsVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "OutstandingLessonsVO.h"
#import "CourseDO.h"
#import "OutstandingLessonVO.h"

@implementation OutstandingLessonsVO

- (id)initWithUser:(UserDO*)userDO; {
    
    self = [super init];
    if (self) {
        
        //build an array of OutstandingLessonVOs, where the last one is the next conversation
        outstandingLessons = [NSMutableArray array];
        
        bool isAllLessonsCompleted = true;
        for (OutstandingLessonDO *outstandingLessonDO in userDO.outstandingLessons) {
            OutstandingLessonVO *outstandingLessonVO = [[OutstandingLessonVO alloc] initWithOutstandingLessonDO:outstandingLessonDO];
            [outstandingLessons addObject:outstandingLessonVO];
            
            if (outstandingLessonVO.status != kOutstandingLessonStatusTypeCompleted) {
                isAllLessonsCompleted = false;
            }
        }
        
        OutstandingLessonStatusType conversationStatusType = kOutstandingLessonStatusTypeLocked;
        if (isAllLessonsCompleted) {
            conversationStatusType = kOutstandingLessonStatusTypeAvailable;
        }
        OutstandingLessonVO *nextConversation = [[OutstandingLessonVO alloc] initWithConversationDO:userDO.currentConversation  withStatusType:conversationStatusType];
        [outstandingLessons addObject:nextConversation];
    }
    return self;
}

- (NSString*)description {return [NSString stringWithFormat:@"Count: %d", outstandingLessons.count];}

- (NSArray*)outstandingLessons {return [NSArray arrayWithArray:outstandingLessons];}

@end
