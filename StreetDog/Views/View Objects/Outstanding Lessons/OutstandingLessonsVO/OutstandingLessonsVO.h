//
//  OutstandingLessonsVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OutstandingLessonVO.h"

@interface OutstandingLessonsVO : NSObject {
    
    NSMutableArray *outstandingLessons;
}

- (id)initWithUser:(UserDO*)userDO;

- (NSArray*)outstandingLessons;

@end
