//
//  UserViewController.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "UserViewController.h"

@implementation UserViewController

#pragma mark - Initialisation
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)backTapped:(id)sender {
    [NavigationController.sharedHelper showHome];
}

- (IBAction)buttonGenderTapped:(UIButton*)sender {
    
    BOOL isMale = true;
    if (sender.tag == 1001) {
        isMale = true;
    } else if (sender.tag == 1002) {
        isMale = false;
    }

    [DataController.sharedHelper.users.currentUser setIsMale:isMale];
    
    [NavigationController.sharedHelper continueStory];
}

@end
