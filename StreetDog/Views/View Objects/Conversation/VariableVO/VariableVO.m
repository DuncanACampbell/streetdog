//
//  VariableVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "VariableVO.h"
#import "CourseDO.h"

@implementation VariableVO

#pragma mark - Initialisation
- (id)initWithVariable:(VariableDefinitionDO*)variableDefinitionDO {
    
    self = [self init];
    if (self) {
        
        identifier = variableDefinitionDO.identifier;
        parameterGroup = variableDefinitionDO.parameterGroup;
        variableValue = variableDefinitionDO.randomValue;
    }
    return self;
}

- (NSString*)description {return identifier;}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (ParameterGroupDO*)parameterGroup {return parameterGroup;}
- (PhraseDO*)value {return variableValue;}

@end
