//
//  VariableVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VariableDefinitionDO;
@class ParameterGroupDO;
@class PhraseDO;

@interface VariableVO : NSObject {
    
    NSString *identifier;
    ParameterGroupDO *parameterGroup;
    PhraseDO *variableValue;
}

- (id)initWithVariable:(VariableDefinitionDO*)variableDefinitionDO;

- (NSString*)identifier;
- (ParameterGroupDO*)parameterGroup;
- (PhraseDO*)value;

@end
