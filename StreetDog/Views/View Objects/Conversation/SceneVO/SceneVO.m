//
//  SceneVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SceneVO.h"
#import "ConversationVO.h"

@implementation SceneVO 

- (id)initWithScene:(SceneDO*)sceneDO withConversation:(ConversationVO*)conversation {
    
    self = [super init];
    if (self) {
        sceneIndex = sceneDO.sceneIndex;
        sceneType = sceneDO.sceneType;
        
        isComplete = false;
        isAlreadyIncorrect = false;
        
        imagePath = sceneDO.imageName;
        
        slideshowImagePaths = [NSMutableArray arrayWithArray:sceneDO.slideshowImages];
        
        helpText = sceneDO.helpText;
        
        //the question and answer phrase instructions should be configured to only return one outcome
        AudioPhraseDO *questionAudioPhrase = [sceneDO.questionPhraseInstruction audioPhraseWithVariables:conversation.variables];
        audioPath = questionAudioPhrase.audioPath;
        audioText = questionAudioPhrase.phrase.text;
        
        AudioPhraseDO *answerAudioPhrase = [sceneDO.answerPhraseInstruction audioPhraseWithVariables:conversation.variables];
        
        if (sceneDO.playAnswerAudio) {
            correctAnswerAudioPath = answerAudioPhrase.audioPath;
        }
        
        correctAnswerText = answerAudioPhrase.phrase.text;
        if (correctAnswerText) NSLog(@"Correct answer: %@", correctAnswerText);
        
        //choose one or two wrong answers
        if (sceneDO.wrongAnswerPhraseInstructions.count > 0) {
            NSMutableArray *potentialWrongAnswers = [NSMutableArray array];
            for (PhraseInstructionDO *wrongAnswerPhraseInstruction in sceneDO.wrongAnswerPhraseInstructions) {
                [potentialWrongAnswers addObjectsFromArray:[wrongAnswerPhraseInstruction audioPhrasesWithVariables:conversation.variables]];
            }
            
            //remove the correct answer
            NSMutableArray *answersToRemove = [NSMutableArray array];
            for (AudioPhraseDO *potentialAnswerText in potentialWrongAnswers) {
                if ([potentialAnswerText.phrase.text isEqualToString:correctAnswerText]) {
                    [answersToRemove addObject:potentialAnswerText];
                }
            }
            [potentialWrongAnswers removeObjectsInArray:answersToRemove];
            
            if (potentialWrongAnswers.count == 0) {
                NSLog(@"No wrong answer phrases found!");
            }
            
            wrongAnswerTexts = [NSMutableArray array];
            for (int i = 0; i < 2; i++) {
                int randomIndex = arc4random() % potentialWrongAnswers.count;
                AudioPhraseDO *wrongAnswerAudioPhrase = [potentialWrongAnswers objectAtIndex:randomIndex];
                [wrongAnswerTexts addObject:wrongAnswerAudioPhrase.phrase.text];
                [potentialWrongAnswers removeObject:wrongAnswerAudioPhrase];
            }
        
            wrongAnswerLessons = [NSMutableArray arrayWithArray:sceneDO.wrongAnswerLessons];
        }
    }
    return self;
}

- (void)reportWrongAnswer {
    
    UserDO *currentUser = DataController.sharedHelper.users.currentUser;
    for (LessonDO *lessonDO in wrongAnswerLessons) {
        [currentUser setOutstandingLesson:lessonDO];
    }
}

- (SceneType)sceneType {return sceneType;}

- (BOOL)isComplete {return isComplete;}
- (void)setComplete {isComplete = true;}

- (BOOL)isAlreadyIncorrect {return isAlreadyIncorrect;}
- (void)setIncorrect {isAlreadyIncorrect = true;}

- (NSString*)audioText {return audioText;}
- (NSString*)audioPath {return audioPath;}
- (NSString*)helpText {return helpText;}

- (NSString*)correctAnswerText {return correctAnswerText;}
- (NSString*)correctAnswerAudioPath {return correctAnswerAudioPath;}
- (NSArray*)wrongAnswerTexts {return [NSArray arrayWithArray:wrongAnswerTexts];}

- (void)setCorrectAnswerButton:(UIButton*)setValue {correctAnswerButton = setValue;}
- (BOOL)isCorrectAnswerButton:(UIButton*)button {return correctAnswerButton == button;}

- (UIImage*)image {
    if (sceneType == kSceneTypeSlideShow) {
        
        //select the next slideshow image and remove it from the array
        if (slideshowImagePaths.count > 0) {
            NSString *thisImagePath = [slideshowImagePaths objectAtIndex:0];
            [slideshowImagePaths removeObjectAtIndex:0];
            return [UIImage imageNamed:thisImagePath];
        } else {
            return nil;
        }
        
    } else {
        return [UIImage imageNamed:imagePath];
    }
}

@end
