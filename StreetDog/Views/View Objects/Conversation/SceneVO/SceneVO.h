//
//  SceneVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"

@class ConversationVO;

@interface SceneVO : NSObject {
    
    int sceneIndex;
    SceneType sceneType;
    
    BOOL isAlreadyIncorrect;
    BOOL isComplete;
    
    NSString *imagePath;
    NSMutableArray *slideshowImagePaths;
    
    NSString *audioText;
    NSString *audioPath;
    
    NSString *helpText;
    
    NSString *correctAnswerText;
    NSString *correctAnswerAudioPath;
    UIButton *correctAnswerButton;
    
    NSMutableArray *wrongAnswerTexts;
    NSMutableArray *wrongAnswerLessons;
}

- (id)initWithScene:(SceneDO*)sceneDO withConversation:(ConversationVO*)conversation;

- (void)reportWrongAnswer;

- (SceneType)sceneType;
- (BOOL)isComplete;
- (void)setComplete;

- (BOOL)isAlreadyIncorrect;
- (void)setIncorrect;

- (UIImage*)image;

- (NSString*)audioText;
- (NSString*)audioPath;
- (NSString*)helpText;

- (NSString*)correctAnswerText;
- (NSString*)correctAnswerAudioPath;

- (void)setCorrectAnswerButton:(UIButton*)setValue;
- (BOOL)isCorrectAnswerButton:(UIButton*)button;

- (NSArray*)wrongAnswerTexts;

@end
