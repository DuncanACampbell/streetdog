//
//  ConversationVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"
#import "SceneVO.h"
#import "VariableVO.h"

@interface ConversationVO : NSObject {
    
    NSString *title;
    NSString *subtitle;
    NSMutableArray *scenes;  //an array of SceneVOs
    NSMutableArray *variables;  //an array of variable values
}

- (id)initWithConversation:(ConversationDO*)conversationDO;

- (NSString*)title;
- (NSString*)subtitle;
- (NSArray*)scenes;
- (NSArray*)variables;

@end
