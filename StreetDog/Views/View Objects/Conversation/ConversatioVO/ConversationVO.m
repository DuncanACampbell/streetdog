//
//  ConversationVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ConversationVO.h"

@implementation ConversationVO

- (id)initWithConversation:(ConversationDO*)conversationDO {
    
    self = [super init];
    if (self) {
        title = conversationDO.title;
        subtitle = conversationDO.subtitle;

        //choose the variables
        variables = [NSMutableArray array];
        for (VariableDefinitionDO *variableDO in conversationDO.variables) {
            VariableVO *variableValue = [[VariableVO alloc] initWithVariable:variableDO];
            [variables addObject:variableValue];
        }
        
        scenes = [NSMutableArray array];
        for (SceneDO *sceneDO in conversationDO.scenes) {
            SceneVO *scene = [[SceneVO alloc] initWithScene:sceneDO withConversation:self];
            [scenes addObject:scene];
        }
    }
    return self;
}

- (NSString*)description {return title;}

- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}
- (NSArray*)scenes {return [NSArray arrayWithArray:scenes];}
- (NSArray*)variables {return [NSArray arrayWithArray:variables];}

@end
