//
//  TranslationPairVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"

@interface TranslationPairVO : NSObject {
    
    NSString *sourceText;
    NSString *sourceNumericText;
    NSString *sourceAudio;
    
    NSString *destinationText;
    NSString *destinationNumericText;
    NSString *destinationAudio;
}

- (id)initWithTranslationPairDO:(TranslationPairDO*)translationPairDO withCourseDO:(CourseDO*)course;

- (NSString*)sourceText;
- (NSString*)sourceNumericText;
- (NSString*)sourceAudio;
- (NSString*)destinationText;
- (NSString*)destinationNumericText;
- (NSString*)destinationAudio;
@end
