//
//  TranslationPairVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "TranslationPairVO.h"

@implementation TranslationPairVO 

- (id)initWithTranslationPairDO:(TranslationPairDO*)translationPairDO withCourseDO:(CourseDO*)course {
    self = [super init];
    if (self) {
        
        sourceText = translationPairDO.sourcePhrase.text;
        sourceNumericText = translationPairDO.sourcePhrase.numericText;
        sourceAudio = [translationPairDO.sourcePhrase getAudioForSourceTutorWithCourse:course];
        
        destinationText = translationPairDO.destinationPhrase.text;
        destinationNumericText = translationPairDO.destinationPhrase.numericText;
        destinationAudio = [translationPairDO.destinationPhrase getAudioForDestinationTutorWithCourse:course];
    }
    return self;
}

- (NSString*)description {return sourceText;}

- (NSString*)sourceText {return sourceText;}
- (NSString*)sourceNumericText {return sourceNumericText;}
- (NSString*)sourceAudio {return sourceAudio;}
- (NSString*)destinationText {return destinationText;}
- (NSString*)destinationNumericText {return destinationNumericText;}
- (NSString*)destinationAudio {return destinationAudio;}

@end
