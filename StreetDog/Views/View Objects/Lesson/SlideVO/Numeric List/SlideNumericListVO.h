//
//  SlideNumericListVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlideVO.h"
#import "CourseDO.h"

@interface SlideNumericListVO : SlideVO {
    
    NSMutableArray *translationPairs;
}

- (id)initWithSlide:(SlideDO*)slideDO;

- (NSArray*)translationPairs;
- (void)removeTranslationPair:(TranslationPairVO*)setValue;

@end
