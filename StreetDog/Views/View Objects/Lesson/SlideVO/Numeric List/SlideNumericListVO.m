//
//  SlideNumericListVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SlideNumericListVO.h"

@implementation SlideNumericListVO 

- (id)initWithSlide:(SlideDO*)slideDO {
    
    self = [super init];
    if (self) {

        translationPairs = [NSMutableArray array];
        for (TranslationPairDO *thisTranslationPairDO in slideDO.translationPairs) {
            TranslationPairVO *thisTranslationPair = [[TranslationPairVO alloc] initWithTranslationPairDO:thisTranslationPairDO withCourseDO:slideDO.course];
            [translationPairs addObject:thisTranslationPair];
        }
    }
    return self;
}

- (NSString*)description {
    TranslationPairVO *firstTranslationPair = [translationPairs objectAtIndex:0];
    return firstTranslationPair.sourceText;
}

- (NSArray*)translationPairs {return [NSArray arrayWithArray:translationPairs];}

- (void)removeTranslationPair:(TranslationPairVO*)setValue {
    [translationPairs removeObject:setValue];
}
@end
