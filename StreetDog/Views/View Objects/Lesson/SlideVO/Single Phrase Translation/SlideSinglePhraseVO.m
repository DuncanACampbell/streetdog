//
//  SlideSinglePhraseVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SlideSinglePhraseVO.h"

@implementation SlideSinglePhraseVO 

- (id)initWithSlide:(SlideDO*)slideDO {
    
    self = [super init];
    if (self) {
        
        helpText = slideDO.helpText;
        
        TranslationPairDO *translationPairDO = [slideDO.translationPairs objectAtIndex:0];
        translationPair = [[TranslationPairVO alloc] initWithTranslationPairDO:translationPairDO withCourseDO:slideDO.course];
    }
    return self;
}

- (NSString*)description {return translationPair.sourceText;}

- (TranslationPairVO*)translationPair {return translationPair;}
- (NSString*)helpText {return helpText;}

@end
