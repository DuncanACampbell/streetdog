//
//  SlideSinglePhraseVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlideVO.h"
#import "CourseDO.h"

@class AudioVO;

@interface SlideSinglePhraseVO : SlideVO {
    
    TranslationPairVO *translationPair;
    NSString *helpText;
}

- (id)initWithSlide:(SlideDO*)slideDO;

- (TranslationPairVO*)translationPair;
- (NSString*)helpText;

@end
