//
//  SlideVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SlideVO.h"

@implementation SlideVO 

- (id)initWithSlide:(SlideDO*)slideDO {
    
    switch (slideDO.slideType) {
        case kSlideTypeSinglePhraseTranslation:
            self = [[SlideSinglePhraseVO alloc] initWithSlide:slideDO]; break;
        case kSlideTypeNumericList:
            self = [[SlideNumericListVO alloc] initWithSlide:slideDO]; break;
        default:
            self = [super init];
    }
    
    slideType = slideDO.slideType;
    isShown = false;
    
    return self;
}

- (SlideType)slideType {return slideType;}
- (BOOL)isShown {return isShown;}
- (void)showSlide {isShown = true;}

@end
