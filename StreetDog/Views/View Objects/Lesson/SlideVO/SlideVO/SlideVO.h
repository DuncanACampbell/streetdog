//
//  SlideVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"
#import "TranslationPairVO.h"

@interface SlideVO : NSObject {
    
    SlideType slideType;
    BOOL isShown;
}

- (id)initWithSlide:(SlideDO*)slideDO;

- (SlideType)slideType;
- (BOOL)isShown;
- (void)showSlide;

@end
