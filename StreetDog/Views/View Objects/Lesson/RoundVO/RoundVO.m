//
//  RoundVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "RoundVO.h"
#import "QuestionVO.h"

@implementation RoundVO 

- (id)initWithRound:(RoundDO*)roundDO withTranslationPairGroups:(NSArray*)translationPairGroups {
    
    self = [super init];
    if (self) {
        
        roundType = roundDO.roundDefinition.roundType;
        
        title = roundDO.roundDefinition.title;
        subtitle = roundDO.roundDefinition.subtitle;
        
        isStarted = false;
        
        //now add one of each of the possible phrases
        questions = [NSMutableArray array];
        lastQuestion = nil;
        
        for (NSArray *thisTranslationPairGroup in translationPairGroups) {
            
            NSMutableArray *allSourceTexts = [NSMutableArray array];
            NSMutableArray *allSourceNumericTexts = [NSMutableArray array];
            NSMutableArray *allDestinationTexts = [NSMutableArray array];
            NSMutableArray *allDestinationNumericTexts = [NSMutableArray array];
            
            for (TranslationPairDO *thisTranslationPair in thisTranslationPairGroup) {
                [allSourceTexts addObject:thisTranslationPair.sourcePhrase.text];
                if (thisTranslationPair.sourcePhrase.numericText) {
                    [allSourceNumericTexts addObject:thisTranslationPair.sourcePhrase.numericText];
                }
                [allDestinationTexts addObject:thisTranslationPair.destinationPhrase.text];
                if (thisTranslationPair.destinationPhrase.numericText) {
                    [allDestinationNumericTexts addObject:thisTranslationPair.destinationPhrase.numericText];
                }
            }
            
            for (TranslationPairDO *thisTranslationPair in thisTranslationPairGroup) {
                
                NSString *questionText;
                NSString *questionAudio;
                NSString *answerText;
                NSString *answerAudio;
                NSArray *wrongAnswerArray;
                int order = thisTranslationPair.sourcePhrase.order;
                
                //depending on the roundType, build the questionVO
                switch (roundType) {
                    case kRoundTypeRecognise:  //destination to destination
                    case kRoundTypeRecogniseOrdered:  //destination to destination
                        questionText = thisTranslationPair.destinationPhrase.text;
                        questionAudio = [thisTranslationPair.destinationPhrase getAudioForDestinationTutorWithCourse:roundDO.course];
                        answerText = questionText;
                        answerAudio = questionAudio;
                        wrongAnswerArray = [self remove:answerText fromArray:allDestinationTexts];
                        break;
                        
                    case kRoundTypeUnderstand: //destination to source
                        questionText = thisTranslationPair.destinationPhrase.text;
                        questionAudio = [thisTranslationPair.destinationPhrase getAudioForDestinationTutorWithCourse:roundDO.course];
                        if (thisTranslationPair.sourcePhrase.numericText) {
                            answerText = thisTranslationPair.sourcePhrase.numericText;
                            wrongAnswerArray = [self remove:answerText fromArray:allSourceNumericTexts];
                        } else {
                            answerText = thisTranslationPair.sourcePhrase.text;
                            wrongAnswerArray = [self remove:answerText fromArray:allSourceTexts];
                        }
                        answerAudio = [thisTranslationPair.sourcePhrase getAudioForSourceTutorWithCourse:roundDO.course];
                        break;
                        
                    case kRoundTypeTranslate:  //source to destination
                    case kRoundTypeTranslateOrdered:  //source to destination
                        if (thisTranslationPair.sourcePhrase.numericText) {
                            questionText = thisTranslationPair.sourcePhrase.numericText;
                        } else {
                            questionText = thisTranslationPair.sourcePhrase.text;
                        }
                        questionAudio = [thisTranslationPair.sourcePhrase getAudioForSourceTutorWithCourse:roundDO.course];
                        answerText = thisTranslationPair.destinationPhrase.text;
                        answerAudio = [thisTranslationPair.destinationPhrase getAudioForDestinationTutorWithCourse:roundDO.course];
                        wrongAnswerArray = [self remove:answerText fromArray:allDestinationTexts];
                        break;
                    case kRoundTypeUnassigned:
                        break;
                }
                
                QuestionVO *thisQuestion = [[QuestionVO alloc] initWithQuestionText:questionText withQuestionAudio:questionAudio withAnswerText:answerText withAnswerAudio:answerAudio withWrongAnswerTexts:wrongAnswerArray withOrder:order];
                [questions addObject:thisQuestion];
                
            }
        }
    }
    return self;
}

- (NSString*)description {return title;}

- (NSArray*)remove:(NSString*)stringToRemove fromArray:(NSArray*)array {
    
    NSMutableArray *returnArray = [NSMutableArray array];
    for (NSString *theString in array) {
        if (![theString isEqualToString:stringToRemove]) {
            [returnArray addObject:theString];
        }
    }
    return [NSArray arrayWithArray:returnArray];
}

- (QuestionVO*)getNextQuestion {
    
    lastQuestion = currentQuestion;
    if (roundType == kRoundTypeRecogniseOrdered || roundType == kRoundTypeTranslateOrdered) {
        
        //select the next incomplete question with the lowest order
        NSArray *sortedArray = [questions sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSNumber *first = [NSNumber numberWithInt:[(QuestionVO*)a order]];
            NSNumber *second = [NSNumber numberWithInt:[(QuestionVO*)b order]];
            return [first compare:second];
        }];
        
        currentQuestion = nil;
        
        int lastQuestionOrder = lastQuestion.order;
        if (!lastQuestion) lastQuestionOrder = -1;
        
        for (QuestionVO *thisQuestion in questions) {
            if (thisQuestion.order == (lastQuestionOrder + 1)) {
                currentQuestion = thisQuestion;
                break;
            }
        }
        
        if (currentQuestion == nil) {
            //if this round is complete, move to the next round
            if (self.isComplete) {
                return nil;
            } else {
                //return the first question from this round
                return [sortedArray objectAtIndex:0];
            }
        }
        
    } else {
        
        //get a list of required questions and choose one of them
        NSMutableArray *potentialNextQuestions = [NSMutableArray array];
        
        bool isFastTrack = true;
        for (QuestionVO *thisQuestion in questions) {
            if (!thisQuestion.isComplete && thisQuestion != lastQuestion) {
                [potentialNextQuestions addObject:thisQuestion];
            }
            
            if (!thisQuestion.isFastTrack) {
                isFastTrack = false;
            }
        }
        
        if (potentialNextQuestions.count < 2 && !isFastTrack) {
            for (QuestionVO *thisQuestion in questions) {
                if (thisQuestion != lastQuestion) {
                    [potentialNextQuestions addObject:thisQuestion];
                }
            }
        }
        
        int rand = arc4random() % potentialNextQuestions.count;
        currentQuestion = [potentialNextQuestions objectAtIndex:rand];
    }
    
    [currentQuestion getAnswerOptions];
    
    return currentQuestion;
}

- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}

- (BOOL)showQuestionText {
    
    switch (roundType) {
        case kRoundTypeRecognise:
        case kRoundTypeRecogniseOrdered:
            return false;
        default:
            return true;
    }
}

- (void)startRound {isStarted = true;}
- (BOOL)isStarted {return isStarted;}

- (BOOL)isComplete {
    
    //we're complete when each question has been answered the required number of times
    for (QuestionVO *thisQuestion in questions) {
        if (!thisQuestion.isComplete) {
            return false;
        }
    }
    return true;
}

- (float)getProgress {
   
    int required = 0;
    int consecutiveCorrects = 0;
    
    for (QuestionVO *thisQuestion in questions) {
        required += thisQuestion.required;
        consecutiveCorrects += thisQuestion.requiredsMet;
    }
    
    return (float)consecutiveCorrects / required;
}

@end
