//
//  RoundVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"
#import "QuestionVO.h"

@interface RoundVO : NSObject {
    
    RoundType roundType;
    NSString *title;
    NSString *subtitle;
    
    BOOL isStarted;
    
    NSMutableArray *questions; //array of QuestionVOs
    QuestionVO *lastQuestion;
    QuestionVO *currentQuestion;
}

- (id)initWithRound:(RoundDO*)roundDO withTranslationPairGroups:(NSArray*)translationPairGroups;

- (NSString*)title;
- (NSString*)subtitle;

- (QuestionVO*)getNextQuestion;
- (BOOL)showQuestionText;

- (void)startRound;
- (BOOL)isStarted;

- (BOOL)isComplete;
- (float)getProgress;

@end
