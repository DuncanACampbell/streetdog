//
//  QuestionVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"

@class AudioVO;

@interface QuestionVO : NSObject {
    
    NSString *questionText;
    NSString *questionAudio;
    
    NSString *answerText;
    NSString *answerAudio;
    
    NSArray *wrongAnswerTexts;
    
    NSArray *answerOptions;
    int correctAnswerOptionIndex;
    
    int order;
    
    bool isFastTrack;
    int required;
    int attempts;
    int corrects;
    int consecutiveCorrects;
}

- (id)initWithQuestionText:(NSString*)theQuestionText withQuestionAudio:(NSString*)theQuestionAudio withAnswerText:(NSString*)theAnswerText withAnswerAudio:(NSString*)theAnswerAudio withWrongAnswerTexts:(NSArray*)theWrongAnswerTexts withOrder:(int)theOrder;

- (NSString *)questionText;
- (NSString *)questionAudio;
- (NSString *)answerText;
- (NSString *)answerAudio;

- (int)order;
- (int)required;
- (int)requiredsMet;

- (BOOL)isFastTrack;

- (NSArray *)getAnswerOptions;
- (BOOL)isAnswerCorrect:(int)answerOptionIndex;
- (BOOL)isComplete;

@end
