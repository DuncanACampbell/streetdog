//
//  QuestionVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "QuestionVO.h"

@implementation QuestionVO

- (id)initWithQuestionText:(NSString*)theQuestionText withQuestionAudio:(NSString*)theQuestionAudio withAnswerText:(NSString*)theAnswerText withAnswerAudio:(NSString*)theAnswerAudio withWrongAnswerTexts:(NSArray*)theWrongAnswerTexts withOrder:(int)theOrder {
    
    self = [super init];
    if (self) {
        
        questionText = theQuestionText;
        questionAudio = theQuestionAudio;
        answerText = theAnswerText;
        answerAudio = theAnswerAudio;
        wrongAnswerTexts = theWrongAnswerTexts;
        
        order = theOrder;
        
        isFastTrack = true;
        required = 1;
        attempts = 0;
        corrects = 0;
        consecutiveCorrects = 0;
    }
    return self;
}

- (NSString*)description {return [NSString stringWithFormat:@"%@ | %@", questionText, answerText];}

- (NSString *)questionText {return questionText;}
- (NSString *)questionAudio {return questionAudio;}
- (NSString *)answerText {return answerText;}
- (NSString *)answerAudio {return answerAudio;}
- (int)order {return order;}
- (int)required {return required;}
- (int)requiredsMet {
    
    if (consecutiveCorrects > required) {
        return required;
    } else {
        return consecutiveCorrects;
    }
}

- (BOOL)isFastTrack {return isFastTrack;}

- (NSArray *)getAnswerOptions {
    
    NSMutableArray *copiedWrongAnswerArray = [wrongAnswerTexts mutableCopy];
    
    //choose one at random
    int rand = arc4random() % copiedWrongAnswerArray.count;
    NSString *firstWrongAnswer = [copiedWrongAnswerArray objectAtIndex:rand];
    [copiedWrongAnswerArray removeObject:firstWrongAnswer];
    
    if (copiedWrongAnswerArray.count > 0) {
        rand = arc4random() % copiedWrongAnswerArray.count;
        NSString *secondWrongAnswer = [copiedWrongAnswerArray objectAtIndex:rand];
        
        //now choose which index will be the correct one
        correctAnswerOptionIndex = arc4random() % 3;
        switch (correctAnswerOptionIndex) {
            case 0:
                answerOptions = [NSArray arrayWithObjects:answerText, firstWrongAnswer, secondWrongAnswer, nil];
                break;
            case 1:
                answerOptions = [NSArray arrayWithObjects:firstWrongAnswer, answerText, secondWrongAnswer, nil];
                break;
            case 2:
                answerOptions = [NSArray arrayWithObjects:firstWrongAnswer, secondWrongAnswer, answerText, nil];
                break;
        }
        
    } else {
        
        //return the two answers
        correctAnswerOptionIndex = arc4random() % 2;
        switch (correctAnswerOptionIndex) {
            case 0:
                answerOptions = [NSArray arrayWithObjects:answerText, firstWrongAnswer, nil];
                break;
            case 1:
                answerOptions = [NSArray arrayWithObjects:firstWrongAnswer, answerText, nil];
                break;
        }
    }

    return answerOptions;
}

- (BOOL)isAnswerCorrect:(int)answerOptionIndex {
    
    attempts++;
    
    if (answerOptionIndex == correctAnswerOptionIndex) {
        corrects++;
        consecutiveCorrects++;
        return true;
    } else {
        isFastTrack = false;
        consecutiveCorrects = 0;
        required = 3;
        return false;
    }
}

- (BOOL)isComplete {
    return (consecutiveCorrects >= required);
}

@end
