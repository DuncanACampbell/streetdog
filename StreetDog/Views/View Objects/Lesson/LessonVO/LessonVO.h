//
//  LessonVO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlideSinglePhraseVO.h"
#import "SlideNumericListVO.h"
#import "RoundVO.h"

@interface LessonVO : NSObject {
    
    LessonDO *lessonDO;
    
    NSString *title;
    NSString *subtitle;
    NSMutableArray *slides;  //an array of SlideVOs
    NSMutableArray *rounds;  //an array of RoundVOs
}

- (id)initWithLesson:(LessonDO*)lessonDO;
- (void)reportCompletedLesson;

- (NSString*)title;
- (NSString*)subtitle;
- (NSArray*)slides;
- (NSArray*)rounds;

@end
