//
//  LessonVO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "LessonVO.h"
#import "CourseDO.h"

@implementation LessonVO

- (id)initWithLesson:(LessonDO*)theLessonDO {
    
    self = [super init];
    if (self) {
        
        lessonDO = theLessonDO;
        
        title = theLessonDO.title;
        subtitle = theLessonDO.subtitle;
        
        slides = [NSMutableArray array];
        for (SlideDO *slideDO in theLessonDO.slides) {
            SlideVO *slide = [[SlideVO alloc] initWithSlide:slideDO];;
            [slides addObject:slide];
        }
        
        rounds = [NSMutableArray array];
        for (RoundDO *roundDO in theLessonDO.rounds) {
            RoundVO *round = [[RoundVO alloc] initWithRound:roundDO withTranslationPairGroups:theLessonDO.translationPairGroups];
            [rounds addObject:round];
        }
        
    }
    return self;
}

- (void)reportCompletedLesson {
    
    UserDO *currentUser = DataController.sharedHelper.users.currentUser;
    [currentUser removeOutstandingLesson:lessonDO];
}

- (NSString*)description {return title;}

- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}
- (NSArray*)slides {return [NSArray arrayWithArray:slides];}
- (NSArray*)rounds {return [NSArray arrayWithArray:rounds];}

@end
