//
//  ConversationViewController.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ConversationViewController.h"

@implementation ConversationViewController

@synthesize viewHolder;
@synthesize viewIntroduction, imageViewIntroduction;
@synthesize viewTitle, labelTitle, labelSubTitle;
@synthesize viewConversation, imageViewConversation, labelQuestionAudio, imageViewLife1, imageViewLife2,imageViewLife3;
@synthesize buttonAnswer1, buttonAnswer2, buttonAnswer3, labelHelpText;
@synthesize viewComplete;

#pragma mark - Initialisation
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    thisAudioController = [[AudioController alloc] initWithDelegate:self];
    
    remainingLifeCount = 3;
    
    //start by showing the introduction view after a short delay
    [self performSelector:@selector(showNextScene) withObject:nil afterDelay:0.5];
}

- (void)setConversationVO:(ConversationVO*)setValue {
    conversationVO = setValue;
}

- (IBAction)backTapped:(id)sender {
    [NavigationController.sharedHelper popController];
}

#pragma mark - Introduction
- (void)showNextScene {
    
    for (SceneVO *thisScene in conversationVO.scenes) {
        if (!thisScene.isComplete) {
            
            currentScene = thisScene;
            
            switch (thisScene.sceneType) {
                    
                case kSceneTypeSlideShow: {
                    UIImage *nextImage = thisScene.image;
                    if (nextImage) {
                        [imageViewIntroduction setImage:nextImage];
                        [viewHolder addSubview:viewIntroduction];
                        [self performSelector:@selector(showNextScene) withObject:nil afterDelay:1.5];
                        return;
                    } else {
                        [thisScene setComplete];
                        [viewIntroduction removeFromSuperview];
                    }
                }
                break;
                    
                case kSceneTypeTitle:
                    [viewHolder addSubview:viewTitle];
                    labelTitle.text = conversationVO.title;
                    labelSubTitle.text = conversationVO.subtitle;
                    [thisScene setComplete];
                    return;
                    break;
                    
                case kSceneTypeAction:
                case kSceneTypeAudio:
                case kSceneTypeAudioAction:
                    
                    //first show the relevant image
                    [imageViewConversation setImage:thisScene.image];
                    [labelHelpText setText:thisScene.helpText];
                    [labelQuestionAudio setText:thisScene.audioText];
                    
                    if (thisScene.sceneType == kSceneTypeAudio) {
                        if (thisScene.audioPath.length > 0) {
                            
                            PostAudioAction thisPostAudioAction;
                            if (thisScene.sceneType == kSceneTypeAudio) {
                                thisPostAudioAction = kPostAudioActionNextScene;
                            } else {
                                thisPostAudioAction = kPostAudioActionShowButtons;
                            }
                            
                            [thisAudioController playAudio:thisScene.audioPath withNextAction:thisPostAudioAction withObject:thisScene];
                        }
                    
                    } else {
                        //this scene has no audio, so go straight for the answers
                        [self showAnswerOptions:thisScene];
                        [viewHolder addSubview:viewConversation];
                    }
                    return;
                    break;
            }
        }
    }
    
    //no more scenes left
    [viewConversation removeFromSuperview];
    [viewHolder addSubview:viewComplete];
}

- (void)showAnswerOptions:(SceneVO*)thisScene {
    
    [buttonAnswer1 setHidden:false];
    [buttonAnswer2 setHidden:false];
    [buttonAnswer3 setHidden:false];
    
    if (thisScene.wrongAnswerTexts.count == 2) {
        int randomIndex = arc4random() % 3;
        switch (randomIndex) {
            case 0:
                [buttonAnswer1 setTitle:thisScene.correctAnswerText forState:UIControlStateNormal];
                [buttonAnswer2 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:0] forState:UIControlStateNormal];
                [buttonAnswer3 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:1] forState:UIControlStateNormal];
                [thisScene setCorrectAnswerButton:buttonAnswer1];
                break;
            case 1:
                [buttonAnswer1 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:0] forState:UIControlStateNormal];
                [buttonAnswer2 setTitle:thisScene.correctAnswerText forState:UIControlStateNormal];
                [buttonAnswer3 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:1] forState:UIControlStateNormal];
                [thisScene setCorrectAnswerButton:buttonAnswer2];
                break;
            case 2:
                [buttonAnswer1 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:0] forState:UIControlStateNormal];
                [buttonAnswer2 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:1] forState:UIControlStateNormal];
                [buttonAnswer3 setTitle:thisScene.correctAnswerText forState:UIControlStateNormal];
                [thisScene setCorrectAnswerButton:buttonAnswer3];
                break;
        }
    } else {
        
        [buttonAnswer1 setHidden:true];
        
        int randomIndex = arc4random() % 2;
        switch (randomIndex) {
            case 0:
                [buttonAnswer2 setTitle:thisScene.correctAnswerText forState:UIControlStateNormal];
                [buttonAnswer3 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:0] forState:UIControlStateNormal];
                [thisScene setCorrectAnswerButton:buttonAnswer2];
                break;
            case 1:
                [buttonAnswer2 setTitle:[thisScene.wrongAnswerTexts objectAtIndex:0] forState:UIControlStateNormal];
                [buttonAnswer3 setTitle:thisScene.correctAnswerText forState:UIControlStateNormal];
                [thisScene setCorrectAnswerButton:buttonAnswer3];
                break;
        }
    }
}

- (IBAction)buttonAnswerTapped:(UIButton*)sender {
    
    //if this is the correct answer, mark the scene as complete
    
    if ([currentScene isCorrectAnswerButton:sender]) {
        
        if (currentScene.correctAnswerAudioPath.length > 0) {
            [thisAudioController playAudio:currentScene.correctAnswerAudioPath withNextAction:kPostAudioActionNextScene withObject:currentScene];
        
        } else {
            [currentScene setComplete];
            [self showNextScene];
        }
    
    } else {
        [sender setHidden:true];
        [self loseOneLife];
        
        if (remainingLifeCount == 0) {
            [viewHolder addSubview:viewComplete];
        }
    }
}

- (void)loseOneLife {
    
    if (!currentScene.isAlreadyIncorrect) {
        
        [currentScene setIncorrect];
        
        remainingLifeCount--;
        
        if (remainingLifeCount < 3) [imageViewLife1 setAlpha:0.2];
        if (remainingLifeCount < 2) [imageViewLife2 setAlpha:0.2];
        if (remainingLifeCount < 1) [imageViewLife3 setAlpha:0.2];
    }
}

- (IBAction)titleNextTapped:(id)sender {
    [viewTitle removeFromSuperview];
    [self showNextScene];
}

#pragma mark - AudioPlayerDelegate
- (void)audioDidFinishPlaying:(PostAudioAction)postAudioAction withObject:(id)objectToReturn {
    
    SceneVO *thisScene = (SceneVO*)objectToReturn;
    
    //do something depending on the action
    switch (postAudioAction) {
        case kPostAudioActionNextScene:
            [thisScene setComplete];
            [self showNextScene];
            break;
            
        case kPostAudioActionShowButtons:
            [self showAnswerOptions:thisScene];
            break;
    }
}

@end
