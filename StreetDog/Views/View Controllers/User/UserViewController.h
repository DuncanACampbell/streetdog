//
//  ConversationViewController.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConversationVO.h"
#import "AudioController.h"

@interface ConversationViewController : UIViewController <AudioControllerDelegate> {
    
    ConversationVO *conversationVO;
    SceneVO *currentScene;
    
    AudioController *thisAudioController;
    
    int remainingLifeCount;
    
    UIView *viewHolder;
    
    UIView *viewIntroduction;
    UIImageView *imageViewIntroduction;
    
    UIView *viewTitle;
    UILabel *labelTitle;
    UILabel *labelSubTitle;
    
    UIView *viewConversation;
    UILabel *labelQuestionAudio;
    UIImageView *imageViewConversation;
    UIImageView *imageViewLife1;
    UIImageView *imageViewLife2;
    UIImageView *imageViewLife3;
    UIButton *buttonAnswer1;
    UIButton *buttonAnswer2;
    UIButton *buttonAnswer3;
    UILabel *labelHelpText;
    
    UIView *viewComplete;
}

@property (nonatomic, retain) IBOutlet UIView *viewHolder;

@property (nonatomic, retain) IBOutlet UIView *viewIntroduction;
@property (nonatomic, retain) IBOutlet UIImageView *imageViewIntroduction;

@property (nonatomic, retain) IBOutlet UIView *viewTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelSubTitle;

@property (nonatomic, retain) IBOutlet UIView *viewConversation;
@property (nonatomic, retain) IBOutlet UILabel *labelQuestionAudio;
@property (nonatomic, retain) IBOutlet UIImageView *imageViewConversation;
@property (nonatomic, retain) IBOutlet UIImageView *imageViewLife1;
@property (nonatomic, retain) IBOutlet UIImageView *imageViewLife2;
@property (nonatomic, retain) IBOutlet UIImageView *imageViewLife3;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer1;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer2;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer3;
@property (nonatomic, retain) IBOutlet UILabel *labelHelpText;

@property (nonatomic, retain) IBOutlet UIView *viewComplete;

- (void)setConversationVO:(ConversationVO*)setValue;

@end
