//
//  HomeViewController.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "HomeViewController.h"
#import "LessonVO.h"
#import "ConversationVO.h"

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)startTapped:(UIButton*)sender {
    
    CourseDO *thisCourse = [DataController.sharedHelper.courses.courses objectAtIndex:0];
    LessonDO *thisLessonDO;
    
    switch (sender.tag) {
        case 0: thisLessonDO = [thisCourse lessonWithID:@"lBasics"]; break;
        case 1: thisLessonDO = [thisCourse lessonWithID:@"lNumbers0to4"]; break;
        case 2: thisLessonDO = [thisCourse lessonWithID:@"lHowMuch1to4"]; break;
        case 3: [DataController.sharedHelper.users addUserWithCourse:thisCourse]; break;
        case 4:
            if (!DataController.sharedHelper.users.currentUser) {
                [DataController.sharedHelper.users addUserWithCourse:thisCourse];
            }
            [NavigationController.sharedHelper continueStory];
            return;
        default: break;
    }

    if (thisLessonDO) {
        LessonVO *thisLessonVO = [[LessonVO alloc] initWithLesson:thisLessonDO];
        [NavigationController.sharedHelper showLesson:thisLessonVO];
    
    }
}

@end
