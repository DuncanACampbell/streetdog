//
//  HomeViewController.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AudioController.h"

@interface HomeViewController : UIViewController

@end
