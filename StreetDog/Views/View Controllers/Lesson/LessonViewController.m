//
//  LessonViewController.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "LessonViewController.h"

@implementation LessonViewController

@synthesize viewHolder;
@synthesize viewIntroduction, labelTitle, labelSubtitle, buttonIntroductionNext;
@synthesize viewSlideSinglePhrase, labelSlideSinglePhraseDestinationText, labelSlideSinglePhraseSourceText, buttonSlideSinglePhraseNext, labelSlideSinglePhraseHelpText;
@synthesize viewSlideNumericList, labelSlideNumericListDestinationText, labelSlideNumericListNumbersText, buttonSlideNumericListNext;
@synthesize viewRoundIntroduction, labelRoundIntroductionSubTitle, labelRoundIntroductionTitle, buttonRoundIntroductionNext;
@synthesize viewRound, labelRoundTitle, labelRoundSubtitle, labelQuestionText, buttonAnswer1, buttonAnswer2, buttonAnswer3, progressViewRound;
@synthesize viewComplete, buttonCompleteNext;

#pragma mark - Initialisation
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    thisAudioController = [[AudioController alloc] initWithDelegate:self];
    
    //start by showing the introduction view after a short delay
    [self performSelector:@selector(showIntroductionView) withObject:nil afterDelay:0.5];
}

- (void)setLessonVO:(LessonVO*)setValue {
    lessonVO = setValue;
}

- (IBAction)backTapped:(id)sender {
    [NavigationController.sharedHelper popController];
}

#pragma mark - Introduction
- (void)showIntroductionView {
    
    labelTitle.text = lessonVO.title;
    labelSubtitle.text = lessonVO.subtitle;
    [viewHolder addSubview:viewIntroduction];
}

- (IBAction)introductionNextTapped:(id)sender {
    
    [viewIntroduction removeFromSuperview];
    [self performSelector:@selector(showSlides) withObject:nil afterDelay:0.5];
}

#pragma mark - Slides
- (void)showSlides {
    
    //show the next slide which hasn't been shown yet
    for (SlideVO *thisSlide in lessonVO.slides) {
        if (!thisSlide.isShown) {
            
            [thisSlide showSlide];
            
            switch (thisSlide.slideType) {
                case kSlideTypeSinglePhraseTranslation: {
                    
                    SlideSinglePhraseVO *singlePhraseSlide = (SlideSinglePhraseVO*)thisSlide;
                    
                    labelSlideSinglePhraseSourceText.text = singlePhraseSlide.translationPair.sourceText;
                    labelSlideSinglePhraseDestinationText.text = singlePhraseSlide.translationPair.destinationText;
                    labelSlideSinglePhraseHelpText.text = singlePhraseSlide.helpText;
                    [buttonSlideSinglePhraseNext setHidden:true];
                    [viewHolder addSubview:viewSlideSinglePhrase];
                    
                    [thisAudioController playAudio:singlePhraseSlide.translationPair.destinationAudio withNextAction:kPostAudioActionShowNext withObject:thisSlide];
                    return;
                }
                
                case kSlideTypeNumericList: {
                    
                    SlideNumericListVO *numericListSlide = (SlideNumericListVO*)thisSlide;

                    //draw the numbers in a line
                    NSMutableString *numbersString = [NSMutableString string];
                    for (TranslationPairVO *translationPair in numericListSlide.translationPairs) {
                        if (numbersString.length != 0) {
                            [numbersString appendString:@" ... "];
                        }
                        [numbersString appendString:translationPair.sourceNumericText];
                    }
                                        
                    labelSlideNumericListNumbersText.text = numbersString;
                    [buttonSlideNumericListNext setHidden:true];
                    [viewHolder addSubview:viewSlideNumericList];
                    
                    //now show each of these destination texts, showing them one after another
                    [self showNumberFromNumericListSlide:numericListSlide];
                    return;
                }
                default:
                    break;
            }
        }
    }
    
    //no slides were found, so move onto rounds
    [self showRounds];
}

- (void)showNumberFromNumericListSlide:(SlideNumericListVO*)slide {
    
    if (slide.translationPairs.count == 0) {
        [slide showSlide];
        [buttonSlideNumericListNext setHidden:false];
        
    } else {
        
        TranslationPairVO *translationPair = [slide.translationPairs objectAtIndex:0];
        labelSlideNumericListDestinationText.text = translationPair.destinationText;
        
        [slide removeTranslationPair:translationPair];
        [thisAudioController playAudio:translationPair.destinationAudio withNextAction:kPostAudioActionNextNumeric withObject:slide];
    }
}

- (IBAction)slideNextTapped:(id)sender {
    
    [viewSlideSinglePhrase removeFromSuperview];
    [viewSlideNumericList removeFromSuperview];
    [self performSelector:@selector(showSlides) withObject:nil afterDelay:0.1];
}

#pragma mark - Rounds
- (void)showRounds {
    
    //show the next round which hasn't been completed yet
    for (RoundVO *thisRound in lessonVO.rounds) {
        if (!thisRound.isComplete) {
            
            if (!thisRound.isStarted) {
                [thisRound startRound];
                labelRoundIntroductionTitle.text = thisRound.title;
                labelRoundIntroductionSubTitle.text = thisRound.subtitle;
                [viewHolder addSubview:viewRoundIntroduction];
                return;
                
            } else {
                [viewRoundIntroduction removeFromSuperview];
                
                //set the progress bar
                progressViewRound.progress = [thisRound getProgress];
                
                labelRoundTitle.text = thisRound.title;
                labelRoundSubtitle.text = thisRound.subtitle;
                
                [labelQuestionText setHidden:false];
                [buttonAnswer1 setHidden:true];
                [buttonAnswer2 setHidden:true];
                [buttonAnswer3 setHidden:true];
                
                currentQuestion = [thisRound getNextQuestion];
                if (!currentQuestion) {
                    break;
                }
                
                if (thisRound.showQuestionText) {
                    labelQuestionText.text = currentQuestion.questionText;
                } else {
                    labelQuestionText.text = [NSString stringWithFormat:@"[%@]", currentQuestion.questionText];
                }
                
                [thisAudioController playAudio:currentQuestion.questionAudio withNextAction:kPostAudioActionShowNext withObject:nil];
                
                NSArray *answerOptionArray = [currentQuestion getAnswerOptions];
                [buttonAnswer1 setTitle:[answerOptionArray objectAtIndex:0] forState:UIControlStateNormal];
                [buttonAnswer2 setTitle:[answerOptionArray objectAtIndex:1] forState:UIControlStateNormal];
                
                if (answerOptionArray.count > 2) {
                    [buttonAnswer3 setTitle:[answerOptionArray objectAtIndex:2] forState:UIControlStateNormal];
                } else {
                    [buttonAnswer3 setHidden:true];
                }
                
                [viewHolder addSubview:viewRound];
                return;
            }
        }
    }
    
    //no incomplete rounds were found, so show complete
    [viewRound removeFromSuperview];
    [self performSelector:@selector(showComplete) withObject:nil afterDelay:0.1];
}

- (IBAction)answerTapped:(UIButton*)sender {
    
    int answerOptionIndex = sender.tag;
    bool isAnswerCorrect = [currentQuestion isAnswerCorrect:answerOptionIndex];
    
    if (isAnswerCorrect) {
        [labelQuestionText setHidden:true];
        [buttonAnswer1 setHidden:true];
        [buttonAnswer2 setHidden:true];
        [buttonAnswer3 setHidden:true];
        [self showRounds];
    
    } else {
        switch (answerOptionIndex) {
            case 0: [buttonAnswer1 setHidden:true]; break;
            case 1: [buttonAnswer2 setHidden:true]; break;
            case 2: [buttonAnswer3 setHidden:true]; break;
        }
    }
}

#pragma mark - Complete
- (void)showComplete {
    [viewHolder addSubview:viewComplete];
}

- (IBAction)completeNextTapped:(id)sender {
    [lessonVO reportCompletedLesson];
    [NavigationController.sharedHelper popController];
}

#pragma mark - AudioPlayerDelegate
- (void)audioDidFinishPlaying:(PostAudioAction)postAudioAction withObject:(id)objectToReturn {
    
    //do something depending on the action
    switch (postAudioAction) {
        case kPostAudioActionShowNext:
            [buttonSlideSinglePhraseNext setHidden:false];
            [buttonSlideNumericListNext setHidden:false];
            
            [buttonAnswer1 setHidden:false];
            [buttonAnswer2 setHidden:false];
            [buttonAnswer3 setHidden:false];
            break;
        
        case kPostAudioActionNextNumeric:
            [self showNumberFromNumericListSlide:objectToReturn];
            break;
            
        default:
            break;
    }
}

@end
