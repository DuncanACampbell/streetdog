//
//  LessonViewController.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LessonVO.h"
#import "AudioController.h"

@interface LessonViewController : UIViewController <AudioControllerDelegate> {
    
    LessonVO *lessonVO;
    QuestionVO *currentQuestion;
    
    AudioController *thisAudioController;
    
    UIView *viewHolder;
    
    UIView *viewIntroduction;
    UILabel *labelTitle;
    UILabel *labelSubtitle;
    UIButton *buttonIntroductionNext;
    
    UIView *viewSlideSinglePhrase;
    UILabel *labelSlideSinglePhraseSourceText;
    UILabel *labelSlideSinglePhraseDestinationText;
    UILabel *labelSlideSinglePhraseHelpText;
    UIButton *buttonSlideSinglePhraseNext;
    
    UIView *viewSlideNumericList;
    UILabel *labelSlideNumericListNumbersText;
    UILabel *labelSlideNumericListDestinationText;
    UIButton *buttonSlideNumericListNext;
    
    UIView *viewRoundIntroduction;
    UILabel *labelRoundIntroductionTitle;
    UILabel *labelRoundIntroductionSubTitle;
    UIButton *buttonRoundIntroductionNext;
    
    UIView *viewRound;
    UILabel *labelRoundTitle;
    UILabel *labelRoundSubTitle;
    UILabel *labelQuestionText;
    UIButton *buttonAnswer1;
    UIButton *buttonAnswer2;
    UIButton *buttonAnswer3;
    UIProgressView *progressViewRound;
    
    UIView *viewComplete;
    UIButton *buttonCompleteNext;
}

@property (nonatomic, retain) IBOutlet UIView *viewHolder;

@property (nonatomic, retain) IBOutlet UIView *viewIntroduction;
@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelSubtitle;
@property (nonatomic, retain) IBOutlet UIButton *buttonIntroductionNext;

@property (nonatomic, retain) IBOutlet UIView *viewSlideSinglePhrase;
@property (nonatomic, retain) IBOutlet UILabel *labelSlideSinglePhraseSourceText;
@property (nonatomic, retain) IBOutlet UILabel *labelSlideSinglePhraseDestinationText;
@property (nonatomic, retain) IBOutlet UILabel *labelSlideSinglePhraseHelpText;
@property (nonatomic, retain) IBOutlet UIButton *buttonSlideSinglePhraseNext;

@property (nonatomic, retain) IBOutlet UIView *viewSlideNumericList;
@property (nonatomic, retain) IBOutlet UILabel *labelSlideNumericListNumbersText;
@property (nonatomic, retain) IBOutlet UILabel *labelSlideNumericListDestinationText;
@property (nonatomic, retain) IBOutlet UIButton *buttonSlideNumericListNext;

@property (nonatomic, retain) IBOutlet UIView *viewRoundIntroduction;
@property (nonatomic, retain) IBOutlet UILabel *labelRoundIntroductionTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelRoundIntroductionSubTitle;
@property (nonatomic, retain) IBOutlet UIButton *buttonRoundIntroductionNext;

@property (nonatomic, retain) IBOutlet UIView *viewRound;
@property (nonatomic, retain) IBOutlet UILabel *labelRoundTitle;
@property (nonatomic, retain) IBOutlet UILabel *labelRoundSubtitle;
@property (nonatomic, retain) IBOutlet UILabel *labelQuestionText;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer1;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer2;
@property (nonatomic, retain) IBOutlet UIButton *buttonAnswer3;
@property (nonatomic, retain) IBOutlet UIProgressView *progressViewRound;

@property (nonatomic, retain) IBOutlet UIView *viewComplete;
@property (nonatomic, retain) IBOutlet UIButton *buttonCompleteNext;

- (void)setLessonVO:(LessonVO*)setValue;

@end
