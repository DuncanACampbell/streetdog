//
//  OutstandingLessonsViewController.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "OutstandingLessonsViewController.h"
#import "OutstandingLessonsVO.h"

@implementation OutstandingLessonsViewController

@synthesize tableView;

#pragma mark - Initialisation
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    outstandingLessonsVO = [[OutstandingLessonsVO alloc] initWithUser:DataController.sharedHelper.users.currentUser];
    [tableView reloadData];
}

- (IBAction)backTapped:(id)sender {
    [NavigationController.sharedHelper popController];
}

- (IBAction)buttonPlayLessonTapped:(UIButton*)sender {
    
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    OutstandingLessonVO *thisOutstandingLessonVO = [outstandingLessonsVO.outstandingLessons objectAtIndex:indexPath.row];
    
    if (thisOutstandingLessonVO.isLesson) {
        LessonVO *thisLessonVO = [[LessonVO alloc] initWithLesson:thisOutstandingLessonVO.lesson];
        [NavigationController.sharedHelper showLesson:thisLessonVO];
    }
    
}

#pragma mark - tableview delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	return [outstandingLessonsVO.outstandingLessons count];
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	UITableViewCell *cell = [tView dequeueReusableCellWithIdentifier: @"tableCell"];
	if (!cell)
        cell = [NavigationController.sharedHelper initialiseTableViewCell:@"OutstandingLessonsCell"];
    
    //leave the first row as it is
    int row = indexPath.row;
    
    //get this tankMeasurement dict
    OutstandingLessonVO *thisLesson = [outstandingLessonsVO.outstandingLessons objectAtIndex:row];
    
    UIButton *buttonPlay = (UIButton *)[cell viewWithTag:1001];
    UILabel *labelTitle = (UILabel *)[cell viewWithTag:1002];
    
    switch (thisLesson.status) {
        case kOutstandingLessonStatusTypeAvailable:
            [buttonPlay setTitle:@"Play" forState:UIControlStateNormal];
            [buttonPlay addTarget:self action:@selector(buttonPlayLessonTapped:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case kOutstandingLessonStatusTypeCompleted:
            [buttonPlay setEnabled:false];
            [buttonPlay setTitle:@"Completed" forState:UIControlStateNormal];
            break;
        case kOutstandingLessonStatusTypeLocked:
            [buttonPlay setEnabled:false];
            [buttonPlay setTitle:@"Locked" forState:UIControlStateNormal];
            break;
    }
    
    [labelTitle setText:thisLesson.title];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
@end
