//
//  OutstandingLessonsViewController.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/4/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OutstandingLessonsVO.h"

@interface OutstandingLessonsViewController : UIViewController {
    
    OutstandingLessonsVO *outstandingLessonsVO;
    
    UITableView *tableView;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@end
