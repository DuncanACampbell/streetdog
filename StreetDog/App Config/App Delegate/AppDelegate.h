//
//  AppDelegate.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

@end
