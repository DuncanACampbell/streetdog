//
//  AudioController.h
//  AppyFeet
//
//  Created by Duncan Campbell on 9/7/12.
//  Copyright (c) 2012 Duncan Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>

@protocol AudioControllerDelegate
- (void) audioDidFinishPlaying:(PostAudioAction)postAudioAction withObject:(id)objectToReturn;
@end

@interface AudioController : NSObject <AVAudioPlayerDelegate> {
    
    id<AudioControllerDelegate> delegate;
    PostAudioAction postAudioAction;
    id objectToReturn;
}

@property (strong,nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) id <AudioControllerDelegate> delegate;

- (id) initWithDelegate:(id<AudioControllerDelegate>)theDelegate;
- (void)playAudio:(NSString*)filename withNextAction:(PostAudioAction)thePostAudioAction withObject:(id)object;

@end
