//
//  AudioController.m
//  AppyFeet
//
//  Created by Duncan Campbell on 9/7/12.
//  Copyright (c) 2012 Duncan Campbell. All rights reserved.
//

#import "AudioController.h"

@implementation AudioController

@synthesize delegate, audioPlayer;

#pragma mark - Instance
- (id) initWithDelegate:(id<AudioControllerDelegate>)theDelegate {
    
    self = [super init];
    if (self) {
        delegate = theDelegate;
    }
    return self;
}

- (void)playAudio:(NSString*)filename withNextAction:(PostAudioAction)thePostAudioAction withObject:(id)object {
    
    postAudioAction = thePostAudioAction;
    objectToReturn = object;
    
    //until this is working
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"m4a"];
    if (soundFilePath) {
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        [self.audioPlayer setDelegate:self];
    } else {
        NSLog(@"No sound file found: %@", filename);
    }
    
#if TARGET_IPHONE_SIMULATOR
    [self audioPlayerDidFinishPlaying:self.audioPlayer successfully:true];
#else
    [self.audioPlayer play];
#endif
    
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)playedSuccessfully {
    self.audioPlayer = nil;
    [self.delegate audioDidFinishPlaying:postAudioAction withObject:objectToReturn];
}
@end
