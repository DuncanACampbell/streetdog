//
//  CourseDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kLanguageTypeSource,
    kLanguageTypeDestination
} LanguageType;

typedef enum {
    kSlideTypeSinglePhraseTranslation,
    kSlideTypeNumericList
} SlideType;

typedef enum {
    kPostAudioActionShowNext,
    kPostAudioActionNextNumeric,
    kPostAudioActionNextScene,
    kPostAudioActionShowButtons,
} PostAudioAction;

typedef enum {
    kRoundTypeUnassigned,
    kRoundTypeRecognise,
    kRoundTypeRecogniseOrdered,
    kRoundTypeUnderstand,
    kRoundTypeTranslate,
    kRoundTypeTranslateOrdered
} RoundType;

typedef enum {
    kGenderUnspecified,
    kGenderMale,
    kGenderFemale
} Gender;

typedef enum {
    kOutstandingLessonStatusTypeLocked,
    kOutstandingLessonStatusTypeAvailable,
    kOutstandingLessonStatusTypeCompleted
} OutstandingLessonStatusType;

@interface EnumDO : NSObject

@end
