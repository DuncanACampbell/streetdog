//
//  NavigationController.m
//  AppyFeet
//
//  Created by Duncan Campbell on 9/7/12.
//  Copyright (c) 2012 Duncan Campbell. All rights reserved.
//

#import "NavigationController.h"
#import "HomeViewController.h"
#import "LessonViewController.h"
#import "ConversationViewController.h"
#import "UserViewController.h"
#import "OutstandingLessonsViewController.h"

@implementation NavigationController

#pragma mark - Singleton
static NavigationController * _sharedHelper;
+ (NavigationController*)sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[NavigationController alloc] init];
    return _sharedHelper;
}

#pragma mark - Instance
static UINavigationController *navigationController;

- (void)setNavigationController:(UINavigationController*)thisNavigationController {
    navigationController = thisNavigationController;
}

- (void)popController {
    [navigationController popViewControllerAnimated:true];
}

- (void)showHome {
    [navigationController popToRootViewControllerAnimated:false];
}

- (void)showLesson:(LessonVO*)lesson {
    LessonViewController *viewController = (LessonViewController*)[self initialiseViewController:[LessonViewController class]];
    [viewController setLessonVO:lesson];
    [navigationController pushViewController:viewController animated:false];
}

- (void)showConversation:(ConversationVO*)conversation {
    
    ConversationViewController *viewController = (ConversationViewController*)[self initialiseViewController:[ConversationViewController class]];
    [viewController setConversationVO:conversation];
    [navigationController pushViewController:viewController animated:false];
}

- (void)showGender {
    UserViewController *viewController = (UserViewController*)[self initialiseViewController:[UserViewController class]];
    [navigationController pushViewController:viewController animated:false];
}

- (void)showOutstandingLessons {
    OutstandingLessonsViewController *viewController = (OutstandingLessonsViewController*)[self initialiseViewController:[OutstandingLessonsViewController class]];
    [navigationController pushViewController:viewController animated:false];
}

- (void)continueStory {
    
    [self showHome];
    
    //if the gender is not yet set, show the gender
    if (DataController.sharedHelper.users.currentUser.gender == kGenderUnspecified) {
        [self showGender];
    
    } else {
        //if any outstanding lessons are available, show that first
        if (DataController.sharedHelper.users.currentUser.outstandingLessons.count > 0) {
            [self showOutstandingLessons];
        
        } else {
            //show the next conversation
            ConversationDO *conversationDO = DataController.sharedHelper.users.currentUser.currentConversation;
            ConversationVO *conversationVO = [[ConversationVO alloc] initWithConversation:conversationDO];
            [self showConversation:conversationVO];
        }
    }
    
}

#pragma mark - Helper
- (UIViewController*)initialiseViewController:(Class)classObject {
    
    NSString *className = NSStringFromClass(classObject);
    
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        className = [NSString stringWithFormat:@"%@_iPad", className];
    }
    
    UIViewController *viewController = [[classObject alloc] initWithNibName:className bundle:nil];
    return viewController;
}

- (UITableViewCell*)initialiseTableViewCell:(NSString*)tableViewCellName {
    
    NSString *cellName = tableViewCellName;
    
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        cellName = [NSString stringWithFormat:@"%@_iPad", tableViewCellName];
    }
    
    UITableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil] lastObject];
    return cell;
}
@end
