//
//  NavigationController.h
//  AppyFeet
//
//  Created by Duncan Campbell on 9/7/12.
//  Copyright (c) 2012 Duncan Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LessonVO.h"
#import "ConversationVO.h"
#import "OutstandingLessonsVO.h"

@interface NavigationController : NSObject

+ (NavigationController*)sharedHelper;

- (void)setNavigationController:(UINavigationController*)thisNavigationController;

- (void)popController;

- (void)showHome;
- (void)showLesson:(LessonVO*)lesson;
- (void)showConversation:(ConversationVO*)conversation;
- (void)showGender;
- (void)showOutstandingLessons;

- (void)continueStory;

- (UIViewController*)initialiseViewController:(Class)classObject;
- (UITableViewCell*)initialiseTableViewCell:(NSString*)tableViewCellName;

@end
