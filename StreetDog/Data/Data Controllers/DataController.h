//
//  DataController.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserController.h"
#import "CourseController.h"
#import "CatalogueController.h"

@interface DataController : NSObject {
    
    CatalogueController *catalogueController;
    int catalogueDataVersionNumber;
    
    UserController *userController;
    
    CourseController *courseController;
    int courseDataVersionNumber;
}

+ (DataController*)sharedHelper;

- (void)loadData;
- (void)updateData;
- (void)saveData;

- (UserController*)users;
- (CourseController*)courses;
- (CatalogueController*)catalogue;

@end
