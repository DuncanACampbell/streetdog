//
//  DataController.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "DataController.h"

@implementation DataController

#pragma mark - Singleton
static DataController * _sharedHelper;
+ (DataController*)sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[DataController alloc] init];
    
    //now update the data
    [_sharedHelper updateData];
    
    return _sharedHelper;
}

#pragma mark - Instance
static NSString* const kCatalogueController = @"catalogueController";
static NSString* const kCatalogueDataVersionNumber = @"catalogueDataVersionNumber";
static NSString* const kUserController = @"userController";
static NSString* const kCourseController = @"courseController";
static NSString* const kCourseDataVersionNumber = @"courseDataVersionNumber";

- (id)init
{
    self = [super init];
    if (self) {
        [self loadData];
    }
    
    return self;
}

- (void)loadData {
    
    //load or initialise the objects
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"userController"]) {
        
        catalogueController = (CatalogueController*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:kCatalogueController]];
        catalogueDataVersionNumber = [[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:kCatalogueDataVersionNumber]] intValue];
        
        userController = (UserController*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:kUserController]];
        
        courseController = (CourseController*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:kCourseController]];
        courseDataVersionNumber = [[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:kCourseDataVersionNumber]] intValue];
        
    } else {
        
        catalogueController = [[CatalogueController alloc] init];
        catalogueDataVersionNumber = 0;
        
        userController = [[UserController alloc] init];
        
        courseController = [[CourseController alloc] init];
        courseDataVersionNumber = 0;
    }
}

- (void)updateData {
    
    //update the catalogue
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CatalogueData" ofType:@"plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        NSLog(@"The catalogue file does not exist");
    } else {
        
        //step through the local Data and add or upgrade the packs
        NSDictionary *localData = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        
        //if the data version number is out of date, update the course data
        bool isDebug = true;
        int thisDataVersionNumber = [[localData objectForKey:kCatalogueDataVersionNumber] intValue];
        if (thisDataVersionNumber != catalogueDataVersionNumber || isDebug) {
            
            [catalogueController updateFromDictionary:localData];
            catalogueDataVersionNumber = thisDataVersionNumber;
        }
    }
    
    path = [[NSBundle mainBundle] pathForResource:@"CourseData" ofType:@"plist"];
    if (![fileManager fileExistsAtPath:path]) {
        NSLog(@"The course file does not exist");
    } else {
        
        //step through the local Data and add or upgrade the packs
        NSDictionary *localData = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        
        //if the data version number is out of date, update the course data
        bool isDebug = true;
        int thisDataVersionNumber = [[localData objectForKey:kCourseDataVersionNumber] intValue];
        if (thisDataVersionNumber != courseDataVersionNumber || isDebug) {
        
            NSArray *courses = [[localData objectForKey:@"courses"] allValues];
            [courseController updateWithArray:courses];
            
            courseDataVersionNumber = thisDataVersionNumber;
        }
    }
    
    [self saveData];
}

- (void)saveData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:catalogueController] forKey:kCatalogueController];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:[NSNumber numberWithInt:catalogueDataVersionNumber]] forKey:kCatalogueDataVersionNumber];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:userController] forKey:kUserController];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:courseController] forKey:kCourseController];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:[NSNumber numberWithInt:courseDataVersionNumber]] forKey:kCourseDataVersionNumber];
    [defaults synchronize];
}

- (CatalogueController*)catalogue {return catalogueController;}
- (UserController*)users {return userController;}
- (CourseController*)courses {return courseController;}

@end
