//
//  TemplateDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "TemplateDO.h"
#import "CourseDO.h"

@implementation TemplateDO

static NSString* const kIdentifier = @"templateID";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict  withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}

@end
