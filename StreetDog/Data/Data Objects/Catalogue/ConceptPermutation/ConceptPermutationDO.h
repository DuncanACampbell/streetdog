//
//  ConceptPermutationDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConceptInstructionDO;
@class LanguageDO;

@interface ConceptPermutationDO : NSObject {
    
    ConceptInstructionDO *parentConceptInstruction;
    NSMutableDictionary *parameterValues;
    NSMutableDictionary *translations;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue withConceptInstruction:(ConceptInstructionDO*)conceptInstruction;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue withConceptInstruction:(ConceptInstructionDO*)conceptInstruction;

- (NSDictionary*)parameterValues;
- (PhraseDO*)translationForLanguage:(LanguageDO*)language;
@end
