//
//  ConceptPermutationDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ConceptPermutationDO.h"

@implementation ConceptPermutationDO

static NSString* const kParameterValues = @"parameterValues";
static NSString* const kTranslations = @"translations";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        parameterValues = [NSMutableDictionary dictionary];
        translations = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        parameterValues = [decoder decodeObjectForKey:kParameterValues];
        translations = [decoder decodeObjectForKey:kTranslations];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:parameterValues forKey:kParameterValues];
    [encoder encodeObject:translations forKey:kTranslations];
}

- (NSString*)description {return parentConceptInstruction.identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue withConceptInstruction:(ConceptInstructionDO*)conceptInstruction {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict withCatalogue:catalogue withConceptInstruction:conceptInstruction];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue withConceptInstruction:(ConceptInstructionDO*)conceptInstruction {
    
    parentConceptInstruction = conceptInstruction;
    
    parameterValues = [NSMutableDictionary dictionary];
    for (NSString *parameterName in [[sourceDict objectForKey:kParameterValues] allKeys]) {
        NSString *parameterValue = [[sourceDict objectForKey:kParameterValues] objectForKey:parameterName];
        PhraseDO *parameterPhrase = [catalogue phraseWithID:parameterValue];
        [parameterValues setObject:parameterPhrase forKey:parameterName];
    }
    
    translations = [NSMutableDictionary dictionary];
    for (NSString *languageID in [[sourceDict objectForKey:@"translations"] allKeys]) {
        NSString *phraseID = [[sourceDict objectForKey:@"translations"] objectForKey:languageID];
        PhraseDO *phrase = [catalogue phraseWithID:phraseID];
        [translations setObject:phrase forKey:languageID];
    }
}

#pragma mark - Getters
- (NSDictionary*)parameterValues {return [NSDictionary dictionaryWithDictionary:parameterValues];}

- (PhraseDO*)translationForLanguage:(LanguageDO*)language {
    return [translations objectForKey:language.identifier];
}
@end
