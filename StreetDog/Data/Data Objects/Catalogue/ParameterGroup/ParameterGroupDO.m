//
//  ParameterGroupDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ParameterGroupDO.h"
#import "CatalogueController.h"

@implementation ParameterGroupDO

static NSString* const kIdentifier = @"parameterGroupID";
static NSString* const kPhrases = @"phrases";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        phrases = [NSMutableArray array];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        phrases = [decoder decodeObjectForKey:kPhrases];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:phrases forKey:kPhrases];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict  withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    phrases = [NSMutableArray array];
    NSArray *thePhrases = [sourceDict objectForKey:kPhrases];
    for (NSString *phraseID in thePhrases) {
        PhraseDO *thisPhrase = [catalogue phraseWithID:phraseID];
        [phrases addObject:thisPhrase];
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSMutableArray*)phrases {return phrases;}

@end
