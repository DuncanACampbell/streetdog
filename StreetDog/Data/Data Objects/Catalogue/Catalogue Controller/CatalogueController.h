//
//  CatalogueController.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoiceDO.h"
#import "PhraseDO.h"
#import "ConceptInstructionDO.h"
#import "LessonDO.h"
#import "RoundDefinitionDO.h"
#import "ParameterGroupDO.h"
#import "LanguageDO.h"

@interface CatalogueController : NSObject {
    
    NSMutableDictionary *voices;
    NSMutableDictionary *phrases;
    NSMutableDictionary *conceptInstructions;
    NSMutableDictionary *parameterGroups;
    NSMutableDictionary *languages;
    NSMutableDictionary *roundDefinitions;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict;
- (void)updateFromDictionary:(NSDictionary*)sourceDict;

- (NSArray*)voices;

- (VoiceDO*)voiceWithID:(NSString*)voiceID;
- (VoiceDO*)tutorVoiceWithLanguage:(LanguageDO*)language;
- (VoiceDO*)userVoiceWithLanguage:(LanguageDO*)language isMale:(BOOL)isMale;

- (PhraseDO*)phraseWithID:(NSString*)phraseID;
- (ConceptInstructionDO*)conceptInstructionWithID:(NSString*)conceptInstructionID;
- (ParameterGroupDO*)parameterGroupWithID:(NSString*)parameterGroupID;
- (LanguageDO*)languageWithID:(NSString*)languageID;
- (RoundDefinitionDO*)roundDefinitionWithID:(NSString*)roundDefinitionID;
@end
