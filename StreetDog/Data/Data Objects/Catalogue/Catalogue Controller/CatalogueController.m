//
//  CatalogueController.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "CatalogueController.h"

@implementation CatalogueController

static NSString* const kVoices = @"voices";
static NSString* const kPhrases = @"phrases";
static NSString* const kConceptInstructions = @"conceptInstructions";
static NSString* const kParameterGroups = @"parameterGroups";
static NSString* const kLanguages = @"languages";
static NSString* const kRoundDefinitions = @"roundDefinitions";

static NSString* const kVoiceID = @"voiceID";
static NSString* const kPhraseID = @"phraseID";
static NSString* const kConceptInstructionID = @"conceptInstructionID";
static NSString* const kParameterGroupID = @"parameterGroupID";
static NSString* const kLanguageID = @"languageID";
static NSString* const kRoundDefinitionID = @"roundDefinitionID";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        
        voices = [NSMutableDictionary dictionary];
        phrases = [NSMutableDictionary dictionary];
        conceptInstructions = [NSMutableDictionary dictionary];
        parameterGroups = [NSMutableDictionary dictionary];
        languages = [NSMutableDictionary dictionary];
        roundDefinitions = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        voices = [decoder decodeObjectForKey:kVoices];
        phrases = [decoder decodeObjectForKey:kPhrases];
        conceptInstructions = [decoder decodeObjectForKey:kConceptInstructions];
        parameterGroups = [decoder decodeObjectForKey:kParameterGroups];
        languages = [decoder decodeObjectForKey:kLanguages];
        roundDefinitions = [decoder decodeObjectForKey:kRoundDefinitions];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:voices forKey:kVoices];
    [encoder encodeObject:phrases forKey:kPhrases];
    [encoder encodeObject:conceptInstructions forKey:kConceptInstructions];
    [encoder encodeObject:parameterGroups forKey:kParameterGroups];
    [encoder encodeObject:languages forKey:kLanguages];
    [encoder encodeObject:roundDefinitions forKey:kRoundDefinitions];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict {
    
    //languages
    NSMutableDictionary *newLanguages = [NSMutableDictionary dictionary];
    for (NSDictionary *languageDict in [[sourceDict objectForKey:kLanguages] allValues]) {
        NSString *languageID = [languageDict objectForKey:kLanguageID];
        LanguageDO *thisLanguage = [self languageExistsWithID:languageID];
        if (thisLanguage) {
            [thisLanguage updateFromDictionary:languageDict withCatalogue:self];
        } else {
            thisLanguage = [[LanguageDO alloc] initFromDictionary:languageDict withCatalogue:self];
        }
        [newLanguages setObject:thisLanguage forKey:languageID];
    }
    languages = newLanguages;
    
    //voices
    NSMutableDictionary *newVoices = [NSMutableDictionary dictionary];
    for (NSDictionary *voiceDict in [[sourceDict objectForKey:kVoices] allValues]) {
        NSString *voiceID = [voiceDict objectForKey:kVoiceID];
        VoiceDO *thisVoice = [self voiceExistsWithID:voiceID];
        if (thisVoice) {
            [thisVoice updateFromDictionary:voiceDict withCatalogue:self];
        } else {
            thisVoice = [[VoiceDO alloc] initFromDictionary:voiceDict withCatalogue:self];
        }
        [newVoices setObject:thisVoice forKey:voiceID];
    }
    voices = newVoices;
    
    //round Definitions
    NSMutableDictionary *newRoundDefinitions = [NSMutableDictionary dictionary];
    for (NSDictionary *roundDefinitionDict in [[sourceDict objectForKey:kRoundDefinitions] allValues]) {
        NSString *roundDefinitionID = [roundDefinitionDict objectForKey:kRoundDefinitionID];
        RoundDefinitionDO *thisRoundDefinition = [self roundDefinitionExistsWithID:roundDefinitionID];
        if (thisRoundDefinition) {
            [thisRoundDefinition updateFromDictionary:roundDefinitionDict withCatalogue:self];
        } else {
            thisRoundDefinition = [[RoundDefinitionDO alloc] initFromDictionary:roundDefinitionDict withCatalogue:self];
        }
        [newRoundDefinitions setObject:thisRoundDefinition forKey:roundDefinitionID];
    }
    roundDefinitions = newRoundDefinitions;
    
    //phrases
    NSMutableDictionary *newPhrases = [NSMutableDictionary dictionary];
    for (NSDictionary *phraseDict in [[sourceDict objectForKey:kPhrases] allValues]) {
        NSString *phraseID = [phraseDict objectForKey:kPhraseID];
        PhraseDO *thisPhrase = [self phraseExistsWithID:phraseID];
        if (thisPhrase) {
            [thisPhrase updateFromDictionary:phraseDict withCatalogue:self];
        } else {
            thisPhrase = [[PhraseDO alloc] initFromDictionary:phraseDict withCatalogue:self];
        }
        [newPhrases setObject:thisPhrase forKey:phraseID];
    }
    phrases = newPhrases;
    
    //parameterGroups
    NSMutableDictionary *newParameterGroups = [NSMutableDictionary dictionary];
    for (NSDictionary *parameterGroupDict in [[sourceDict objectForKey:kParameterGroups] allValues]) {
        NSString *parameterGroupID = [parameterGroupDict objectForKey:kParameterGroupID];
        ParameterGroupDO *thisParameterGroup = [self parameterGroupExistsWithID:parameterGroupID];
        if (thisParameterGroup) {
            [thisParameterGroup updateFromDictionary:parameterGroupDict withCatalogue:self];
        } else {
            thisParameterGroup = [[ParameterGroupDO alloc] initFromDictionary:parameterGroupDict withCatalogue:self];
        }
        [newParameterGroups setObject:thisParameterGroup forKey:parameterGroupID];
    }
    parameterGroups = newParameterGroups;
    
    //conceptInstructions
    NSMutableDictionary *newConceptInstructions = [NSMutableDictionary dictionary];
    for (NSDictionary *conceptInstructionDict in [[sourceDict objectForKey:kConceptInstructions] allValues]) {
        NSString *conceptInstructionID = [conceptInstructionDict objectForKey:kConceptInstructionID];
        ConceptInstructionDO *thisConceptInstruction = [self conceptInstructionExistsWithID:conceptInstructionID];
        if (thisConceptInstruction) {
            [thisConceptInstruction updateFromDictionary:conceptInstructionDict withCatalogue:self];
        } else {
            thisConceptInstruction = [[ConceptInstructionDO alloc] initFromDictionary:conceptInstructionDict withCatalogue:self];
        }
        [newConceptInstructions setObject:thisConceptInstruction forKey:conceptInstructionID];
    }
    conceptInstructions = newConceptInstructions;
}

#pragma mark - Getters
- (NSArray*)voices {return [NSArray arrayWithArray:voices.allValues];}

#pragma mark - Getters (exists)
- (VoiceDO*)voiceExistsWithID:(NSString*)voiceID {
    return [voices objectForKey:voiceID];
}
- (PhraseDO*)phraseExistsWithID:(NSString*)phraseID {
    return [phrases objectForKey:phraseID];
}
- (ConceptInstructionDO*)conceptInstructionExistsWithID:(NSString*)conceptInstructionID {
    return [conceptInstructions objectForKey:conceptInstructionID];
}
- (ParameterGroupDO*)parameterGroupExistsWithID:(NSString*)parameterGroupID {
    return [parameterGroups objectForKey:parameterGroupID];
}
- (LanguageDO*)languageExistsWithID:(NSString*)languageID {
    return [languages objectForKey:languageID];
}
- (RoundDefinitionDO*)roundDefinitionExistsWithID:(NSString*)roundDefinitionID {
    return [roundDefinitions objectForKey:roundDefinitionID];
}

#pragma mark - Getters (external)
- (VoiceDO*)voiceWithID:(NSString*)voiceID {
    VoiceDO *returnValue = [voices objectForKey:voiceID];
    if (!returnValue) [self objectNotFound:voiceID];
    return returnValue;
}
- (PhraseDO*)phraseWithID:(NSString*)phraseID {
    PhraseDO *returnValue = [phrases objectForKey:phraseID];
    if (!returnValue) [self objectNotFound:phraseID];
    return returnValue;
}
- (ConceptInstructionDO*)conceptInstructionWithID:(NSString*)conceptInstructionID {
    ConceptInstructionDO *returnValue = [conceptInstructions objectForKey:conceptInstructionID];
    if (!returnValue) [self objectNotFound:conceptInstructionID];
    return returnValue;
}
- (ParameterGroupDO*)parameterGroupWithID:(NSString*)parameterGroupID {
    ParameterGroupDO *returnValue = [parameterGroups objectForKey:parameterGroupID];
    if (!returnValue) [self objectNotFound:parameterGroupID];
    return returnValue;
}
- (LanguageDO*)languageWithID:(NSString*)languageID {
    LanguageDO *returnValue = [languages objectForKey:languageID];
    if (!returnValue) [self objectNotFound:languageID];
    return returnValue;
}
- (RoundDefinitionDO*)roundDefinitionWithID:(NSString*)roundDefinitionID {
    RoundDefinitionDO *returnValue = [roundDefinitions objectForKey:roundDefinitionID];
    if (!returnValue) [self objectNotFound:roundDefinitionID];
    return returnValue;
}

- (VoiceDO*)tutorVoiceWithLanguage:(LanguageDO*)language {
    for (VoiceDO *thisVoice in [voices allValues]) {
        if (thisVoice.isTutor && thisVoice.language == language) {
            return thisVoice;
        }
    }
    
    //no matching tutor found
    return nil;
}

- (VoiceDO*)userVoiceWithLanguage:(LanguageDO*)language isMale:(BOOL)isMale {
    for (VoiceDO *thisVoice in [voices allValues]) {
        if (!thisVoice.isTutor && thisVoice.language == language && thisVoice.isMale == isMale) {
            return thisVoice;
        }
    }
    
    //no matching tutor found
    return nil;
}

#pragma mark - errors
- (void)objectNotFound:(NSString*)objectID {
    NSLog(@"Object not found: %@", objectID);
}
@end
