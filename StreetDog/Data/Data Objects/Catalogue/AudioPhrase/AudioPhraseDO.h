//
//  AudioPhraseDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PhraseDO;

@interface AudioPhraseDO : NSObject {
    
    PhraseDO *phrase;
    NSString *audioPath;
}

- (id)initFromConceptPermutation:(ConceptPermutationDO*)theConceptPermutation withVoice:(VoiceDO*)voice;

- (PhraseDO*)phrase;
- (NSString*)audioPath;

@end
