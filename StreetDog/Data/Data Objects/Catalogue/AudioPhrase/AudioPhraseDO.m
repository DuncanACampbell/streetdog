//
//  AudioPhraseDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "AudioPhraseDO.h"
#import "CourseDO.h"

@implementation AudioPhraseDO

- (NSString*)description {return phrase.text;}

- (id)initFromConceptPermutation:(ConceptPermutationDO*)theConceptPermutation withVoice:(VoiceDO*)voice {
    
    self = [self init];
    if (self) {
        
        //find the translation which matches the sourceLanguage
        phrase = [theConceptPermutation translationForLanguage:voice.language];
        audioPath = [phrase getAudioForVoice:voice];
    }
    return self;
}

#pragma mark - Getters
- (PhraseDO*)phrase {return phrase;}
- (NSString*)audioPath {return audioPath;}

@end
