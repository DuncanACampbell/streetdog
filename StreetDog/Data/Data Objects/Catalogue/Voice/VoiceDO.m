//
//  VoiceDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "VoiceDO.h"

@implementation VoiceDO

static NSString* const kIdentifier = @"voiceID";
static NSString* const kTitle = @"title";
static NSString* const kLanguage = @"language";
static NSString* const kIsTutor = @"isTutor";
static NSString* const kIsUser = @"isUser";
static NSString* const kIsMale = @"isMale";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
        language = nil;
        isTutor = false;
        isUser = false;
        isMale = false;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        language = [decoder decodeObjectForKey:kLanguage];
        isTutor = [[decoder decodeObjectForKey:kIsTutor] boolValue];
        isUser = [[decoder decodeObjectForKey:kIsUser] boolValue];
        isMale = [[decoder decodeObjectForKey:kIsMale] boolValue];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:language forKey:kLanguage];
    [encoder encodeObject:[NSNumber numberWithBool:isTutor] forKey:kIsTutor];
    [encoder encodeObject:[NSNumber numberWithBool:isUser] forKey:kIsUser];
    [encoder encodeObject:[NSNumber numberWithBool:isMale] forKey:kIsMale];
}

- (NSString*)description {return title;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    title = [sourceDict objectForKey:kTitle];
    isTutor = [[sourceDict objectForKey:kIsTutor] boolValue];
    isUser = [[sourceDict objectForKey:kIsUser] boolValue];
    isMale = [[sourceDict objectForKey:kIsMale] boolValue];
    
    NSString *languageID = [sourceDict objectForKey:@"languageID"];
    language = [catalogue languageWithID:languageID];
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)title {return title;}
- (LanguageDO*)language {return language;}
- (BOOL)isTutor {return isTutor;}
- (BOOL)isUser {return isUser;}
- (BOOL)isMale {return isMale;}

@end
