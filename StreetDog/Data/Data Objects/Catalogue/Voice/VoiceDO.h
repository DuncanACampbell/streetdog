//
//  VoiceDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumDO.h"

@class CatalogueController;
@class LanguageDO;

@interface VoiceDO : NSObject {
    
    NSString *identifier;
    NSString *title;
    LanguageDO *language;
    BOOL isTutor;
    BOOL isUser;
    BOOL isMale;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;

- (NSString*)identifier;
- (NSString*)title;
- (LanguageDO*)language;
- (BOOL)isTutor;
- (BOOL)isUser;
- (BOOL)isMale;

@end
