//
//  RoundDefinitionDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "RoundDefinitionDO.h"
#import "CatalogueController.h"

@implementation RoundDefinitionDO

static NSString* const kIdentifier = @"roundDefinitionID";
static NSString* const kTitle = @"title";
static NSString* const kSubtitle = @"subtitle";
static NSString* const kRoundType = @"roundType";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
        subtitle = @"";
        roundType = kRoundTypeUnassigned;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        subtitle = [decoder decodeObjectForKey:kSubtitle];
        roundType = [[decoder decodeObjectForKey:kRoundType] intValue];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:subtitle forKey:kSubtitle];
    [encoder encodeObject:[NSNumber numberWithInt:roundType] forKey:kRoundType];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    title = [sourceDict objectForKey:kTitle];
    subtitle = [sourceDict objectForKey:kSubtitle];
    
    if ([identifier isEqualToString:@"rRecognise"]) {
        roundType = kRoundTypeRecognise;
    } else if ([identifier isEqualToString:@"rRecogniseOrdered"]) {
        roundType = kRoundTypeRecogniseOrdered;
    } else if ([identifier isEqualToString:@"rUnderstand"]) {
        roundType = kRoundTypeUnderstand;
    } else if ([identifier isEqualToString:@"rTranslate"]) {
        roundType = kRoundTypeTranslate;
    } else if ([identifier isEqualToString:@"rTranslateOrdered"]) {
        roundType = kRoundTypeTranslateOrdered;
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}
- (RoundType)roundType {return roundType;}

@end
