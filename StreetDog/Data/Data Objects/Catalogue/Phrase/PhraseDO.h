//
//  PhraseDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoiceDO.h"

@class CatalogueController;
@class CourseDO;

@interface PhraseDO : NSObject {
    
    NSString *identifier;
    NSString *text;
    NSString *punctuatedText;
    NSString *numericText;
    NSMutableArray *languages;
    int order;
    NSMutableDictionary *audiosByVoice;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;

- (NSString*)identifier;
- (NSString*)text;
- (int)order;
- (NSString*)punctuatedText;
- (NSString*)numericText;

- (NSString*)getAudioForVoice:(VoiceDO*)voice;
- (NSString*)getAudioForSourceTutorWithCourse:(CourseDO*)course;
- (NSString*)getAudioForDestinationTutorWithCourse:(CourseDO*)course;

@end
