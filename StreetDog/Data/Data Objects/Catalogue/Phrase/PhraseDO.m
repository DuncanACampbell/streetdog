//
//  PhraseDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "PhraseDO.h"
#import "CatalogueController.h"

@implementation PhraseDO

static NSString* const kIdentifier = @"phraseID";
static NSString* const kText = @"text";
static NSString* const kPunctuatedText = @"punctuatedText";
static NSString* const kNumericText = @"numericText";
static NSString* const kOrder = @"order";
static NSString* const kLanguageType = @"language";
static NSString* const kLanguages = @"languages";
static NSString* const kAudiosByVoice = @"audiosByVoice";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        text = @"";
        punctuatedText = @"";
        numericText = @"";
        order = 0;
        languages = [NSMutableArray array];
        audiosByVoice = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        text = [decoder decodeObjectForKey:kText];
        punctuatedText = [decoder decodeObjectForKey:kPunctuatedText];
        numericText = [decoder decodeObjectForKey:kNumericText];
        order = [[decoder decodeObjectForKey:kOrder] intValue];
        languages = [decoder decodeObjectForKey:kLanguages];
        audiosByVoice = [decoder decodeObjectForKey:kAudiosByVoice];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:text forKey:kText];
    [encoder encodeObject:punctuatedText forKey:kPunctuatedText];
    [encoder encodeObject:numericText forKey:kNumericText];
    [encoder encodeObject:[NSNumber numberWithInt:order] forKey:kOrder];
    [encoder encodeObject:languages forKey:kLanguages];
    [encoder encodeObject:audiosByVoice forKey:kAudiosByVoice];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    text = [sourceDict objectForKey:kText];
    punctuatedText = [NSString stringWithFormat:@"%@.", text];
    numericText = [sourceDict objectForKey:kNumericText];
    
    order = [[sourceDict objectForKey:kOrder] intValue];
    
    languages = [NSMutableArray array];
    for (NSString *languageID in [sourceDict objectForKey:kLanguages]) {
        LanguageDO *thisLanguage = [catalogue languageWithID:languageID];
        [languages addObject:thisLanguage];
    }
    
    //add an audio for each voice of the same language
    audiosByVoice = [NSMutableDictionary dictionary];
    for (VoiceDO *thisVoice in catalogue.voices) {
        if ([languages containsObject:thisVoice.language]) {
            NSString *audioPath = [NSString stringWithFormat:@"a_%@_%@", identifier, thisVoice.identifier];
            [audiosByVoice setObject:audioPath forKey:thisVoice.identifier];
        }
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)text {return text;}
- (NSString*)punctuatedText {return punctuatedText;}
- (NSString*)numericText {return numericText;}
- (int)order {return order;}

- (NSString*)getAudioForVoiceID:(NSString*)voiceID {
    
    NSString *returnAudio = [audiosByVoice objectForKey:voiceID];
    if (!returnAudio) {
        NSLog(@"Audio for voice not found");
    }
    return returnAudio;
}

- (NSString*)getAudioForVoice:(VoiceDO*)voice {
    return [self getAudioForVoiceID:voice.identifier];
}

- (NSString*)getAudioForSourceTutorWithCourse:(CourseDO*)course {
    
    //get the voiceID for the source language's tutor
    NSString *sourceVoiceID = @"vTutorEnglishUK";
    return [self getAudioForVoiceID:sourceVoiceID];
}

- (NSString*)getAudioForDestinationTutorWithCourse:(CourseDO*)course  {
    NSString *destinationVoiceID = @"vTutorEspañol";
    return [self getAudioForVoiceID:destinationVoiceID];
}
@end
