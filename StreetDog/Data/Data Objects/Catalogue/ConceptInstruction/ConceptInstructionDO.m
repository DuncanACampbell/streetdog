//
//  ConceptInstructionDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ConceptInstructionDO.h"
#import "CatalogueController.h"

@implementation ConceptInstructionDO

static NSString* const kIdentifier = @"conceptInstructionID";
static NSString* const kTitle = @"title";
static NSString* const kParameterGroups = @"parameterGroups";
static NSString* const kPermutations = @"permutations";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
        parameterGroups = [NSMutableArray array];
        permutations = [NSMutableArray array];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        parameterGroups = [decoder decodeObjectForKey:kParameterGroups];
        permutations = [decoder decodeObjectForKey:kPermutations];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:parameterGroups forKey:kParameterGroups];
    [encoder encodeObject:permutations forKey:kPermutations];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    title = [sourceDict objectForKey:kTitle];
    parentCatalogue = catalogue;
    
    parameterGroups = [NSMutableArray array];
    NSArray *theParameterGroups = [sourceDict objectForKey:kParameterGroups];
    for (NSString *parameterGroupID in theParameterGroups) {
        ParameterGroupDO *thisParameterGroup = [catalogue parameterGroupWithID:parameterGroupID];
        [parameterGroups addObject:thisParameterGroup];
    }
    
    permutations = [NSMutableArray array];
    NSArray *thePermutations = [sourceDict objectForKey:kPermutations];
    for (NSDictionary *permutation in thePermutations) {
        
        ConceptPermutationDO *thisPermutation = [[ConceptPermutationDO alloc] initFromDictionary:permutation withCatalogue:catalogue withConceptInstruction:self];
        [permutations addObject:thisPermutation];
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSArray*)parameterGroups {return [NSArray arrayWithArray:parameterGroups];}
- (NSArray*)allPermutations {return [NSArray arrayWithArray:permutations];}

//- (ConceptPermutationDO*)conceptPermutationWithParameterDict:(NSDictionary*)parameters {
//    
//    //if we have any parameter groups, we need find the right match
//    if (parameterGroups.count > 0) {
//        
//        //loop through each permutation
//        for (ConceptPermutationDO *thisConceptPermutation in permutations) {
//            
//            //do all the parameters match
//            bool isFullMatch = true;
//            for (NSString *tpParameterGroupID in thisConceptPermutation.parameterValues.allKeys) {
//                PhraseDO *tpParameterPhraseDO = [thisConceptPermutation.parameterValues objectForKey:tpParameterGroupID];
//                NSString *inputParameterPhraseID = [parameters objectForKey:tpParameterGroupID];
//                if (![tpParameterPhraseDO.identifier isEqualToString:inputParameterPhraseID]) {
//                    isFullMatch = false;
//                    break;
//                }
//            }
//            
//            if (isFullMatch) {
//                return  thisConceptPermutation;
//            }
//        }
//        
//        //no matching conceptPermutation was found, so return nil
//        return nil;
//    
//    } else {
//    
//        //no parameter groups, so there is just one conceptPermutation to return
//        return [permutations objectAtIndex:0];
//    }
//}
//
//- (NSArray*)conceptPermutationsWithFilterGroups:(NSDictionary*)filterGroups {
//    
//    //if we have any filter groups, we only want to return items which fall inside the group
//    if (filterGroups.count > 0) {
//        
//        //loop through each permutation of this conceptInstruction
//        NSMutableArray *filteredPhrases = [NSMutableArray array];
//        for (ConceptPermutationDO *thisConceptPermutation in permutations) {
//            
//            //collect the phrases which exist in the filter group
//            bool isFullMatch = true;
//            for (NSString *tpParameterGroupID in thisConceptPermutation.parameterValues.allKeys) {
//                PhraseDO *tpParameterPhraseDO = [thisConceptPermutation.parameterValues objectForKey:tpParameterGroupID];
//                
//                //does the phrase above (tpParameterPhraseDO) fall inside our passed in group for the same key
//                NSString *parameterGroupID = [filterGroups objectForKey:tpParameterGroupID];
//                if (!parameterGroupID) {
//                    NSLog(@"Catalogue.phraseIntruction.filterGroup %@ not found on course.lesson.conceptInstruction", tpParameterGroupID);
//                }
//                ParameterGroupDO *filterGroupDO = [parentCatalogue parameterGroupWithID:parameterGroupID];
//                
//                if (![filterGroupDO.phrases containsObject:tpParameterPhraseDO]) {
//                    isFullMatch = false;
//                    break;
//                }
//            }
//            
//            if (isFullMatch) {
//                [filteredPhrases addObject:thisConceptPermutation];;
//            }
//        }
//        
//        //no matching conceptPermutation was found, so return nil
//        return filteredPhrases;
//        
//    } else {
//        
//        //no parameter groups, so there is just one conceptPermutation to return
//        return [NSArray arrayWithArray:permutations];
//    }
//}

@end
