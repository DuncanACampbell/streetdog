//
//  ConceptInstructionDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhraseDO.h"
#import "ConceptPermutationDO.h"
#import "TranslationPairDO.h"

@class CatalogueController;

@interface ConceptInstructionDO : NSObject {
    
    NSString *identifier;
    NSString *title;
    NSMutableArray *parameterGroups;
    NSMutableArray *permutations;
    
    CatalogueController *parentCatalogue;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;

- (NSString*)identifier;
- (NSArray*)parameterGroups;
- (NSArray*)allPermutations;

//- (ConceptPermutationDO*)conceptPermutationWithParameterDict:(NSDictionary*)parameters;
//- (NSArray*)conceptPermutationsWithFilterGroups:(NSDictionary*)filterGroups;

@end
