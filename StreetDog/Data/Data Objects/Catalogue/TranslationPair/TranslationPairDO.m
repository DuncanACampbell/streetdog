//
//  TranslationPairDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "TranslationPairDO.h"
#import "ConceptPermutationDO.h"
#import "PhraseInstructionDO.h"
#import "PhraseDO.h"
#import "LanguageDO.h"
#import "AudioPhraseDO.h"

@implementation TranslationPairDO

- (NSString*)description {return sourceAudioPhrase.phrase.text;}

- (id)initFromConceptPermutation:(ConceptPermutationDO*)theConceptPermutation withPhraseInstruction:(PhraseInstructionDO*)phraseInstruction {
    
    self = [self init];
    if (self) {
        
        //find the translation which matches the sourceLanguage
        sourceAudioPhrase = [[AudioPhraseDO alloc] initFromConceptPermutation:theConceptPermutation withVoice:phraseInstruction.sourceVoice];
        
        destinationAudioPhrase = [[AudioPhraseDO alloc] initFromConceptPermutation:theConceptPermutation withVoice:phraseInstruction.destinationVoice];
    }
    return self;
}

#pragma mark - Getters
- (PhraseDO*)sourcePhrase {return sourceAudioPhrase.phrase;}
- (PhraseDO*)destinationPhrase {return destinationAudioPhrase.phrase;}
- (NSDictionary*)parameterValues {return [NSDictionary dictionaryWithDictionary:conceptPermutation.parameterValues];}

@end
