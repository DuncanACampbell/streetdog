//
//  TranslationPairDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioPhraseDO.h"

@class ConceptPermutationDO;
@class PhraseInstructionDO;
@class AudioPhraseDO;
@class PhraseDO;

@interface TranslationPairDO : NSObject {
    
    ConceptPermutationDO *conceptPermutation;
    
    AudioPhraseDO *sourceAudioPhrase;
    AudioPhraseDO *destinationAudioPhrase;
}

- (id)initFromConceptPermutation:(ConceptPermutationDO*)theConceptPermutation withPhraseInstruction:(PhraseInstructionDO*)phraseInstruction;

- (PhraseDO*)sourcePhrase;
- (PhraseDO*)destinationPhrase;
- (NSDictionary*)parameterValues;

@end
