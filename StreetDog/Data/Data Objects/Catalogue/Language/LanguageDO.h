//
//  LanguageDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageDO : NSObject {
    
    NSString *identifier;
    NSString *title;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue;

- (NSString*)identifier;
- (NSString*)title;

@end
