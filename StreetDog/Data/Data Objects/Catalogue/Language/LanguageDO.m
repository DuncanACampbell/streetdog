//
//  LanguageDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "LanguageDO.h"
#import "CourseDO.h"

@implementation LanguageDO

static NSString* const kIdentifier = @"languageID";
static NSString* const kTitle = @"title";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCatalogue:catalogue];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCatalogue:(CatalogueController*)catalogue {
    title = [sourceDict objectForKey:kTitle];
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)title {return title;}
@end
