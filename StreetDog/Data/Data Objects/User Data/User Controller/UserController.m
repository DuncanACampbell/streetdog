//
//  UserController.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "UserController.h"

@implementation UserController

static NSString* const kUsers = @"users";
static NSString* const kCurrentUser = @"currentUser";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        users = [NSMutableDictionary dictionary];
        currentUser = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        users = [decoder decodeObjectForKey:kUsers];
        currentUser = [decoder decodeObjectForKey:kCurrentUser];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:users forKey:kUsers];
    [encoder encodeObject:currentUser forKey:kCurrentUser];
}

#pragma mark - Getters
- (NSArray*)users {return [users allValues];}
- (UserDO*)currentUser {return currentUser;}

#pragma mark - Setters
- (UserDO*)addUserWithCourse:(CourseDO*)course {
    
    UserDO *newUser = [[UserDO alloc] initWithCourse:course];
    [users setObject:newUser forKey:newUser.identifier];
    
    [self setCurrentUser:newUser];
    
    return newUser;
}

- (void)deleteUser:(UserDO*)user {
    
    if (currentUser == user) {
        currentUser = nil;
    }
    
    if ([[users allValues] containsObject:user]) {
        [users removeObjectForKey:user.identifier];
    }
    [DataController.sharedHelper saveData];
}

- (void)setCurrentUser:(UserDO*)user {
    currentUser = user;
}

@end
