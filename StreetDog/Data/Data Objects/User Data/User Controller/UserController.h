//
//  UserController.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDO.h"

@interface UserController : NSObject {
    
    NSMutableDictionary *users;
    UserDO *currentUser;
}

- (UserDO*)addUserWithCourse:(CourseDO*)course;
- (void)deleteUser:(UserDO*)user;

- (UserDO*)currentUser;
- (void)setCurrentUser:(UserDO*)user;

@end
