//
//  OutstandingLessonDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 11/3/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OutstandingLessonDO : NSObject {
    
    LessonDO *lesson;
    OutstandingLessonStatusType status;
}

- (id)initWithLesson:(LessonDO*)lesson;

- (LessonDO*)lesson;
- (OutstandingLessonStatusType)status;

- (void)setCompleted;

@end
