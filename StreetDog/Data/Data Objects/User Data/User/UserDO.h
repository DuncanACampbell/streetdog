//
//  UserDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"
#import "OutstandingLessonDO.h"

@interface UserDO : NSObject {
    
    NSString *identifier;
    
    CourseDO *course;
    
    ConversationDO *currentConversation;
    NSMutableArray *outstandingLessons;

    Gender gender;
}

- (NSString*)identifier;

- (Gender)gender;
- (void)setIsMale:(BOOL)setValue;

- (ConversationDO*)currentConversation;

- (NSArray*)outstandingLessons;
- (void)setOutstandingLesson:(LessonDO*)lesson;
- (void)removeOutstandingLesson:(LessonDO*)lesson;

- (id)initWithCourse:(CourseDO*)theCourse;

@end
