//
//  TemplateDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CourseDO;

@interface TemplateDO : NSObject {
    
    NSString *identifier;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (NSString*)identifier;

@end
