//
//  RoundDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "RoundDO.h"
#import "CatalogueController.h"

@implementation RoundDO

static NSString* const kRoundDefinition = @"roundDefinition";
static NSString* const kParentCourse = @"parentCourse";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        roundDefinition = nil;
        parentCourse = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        roundDefinition = [decoder decodeObjectForKey:kRoundDefinition];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:roundDefinition forKey:kRoundDefinition];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
}

- (NSString*)description {return roundDefinition.title;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    NSString *roundDefintionID = [sourceDict objectForKey:@"roundDefinitionID"];
    roundDefinition = [DataController.sharedHelper.catalogue roundDefinitionWithID:roundDefintionID];
    
    parentCourse = course;
}

#pragma mark - Getters
- (RoundDefinitionDO*)roundDefinition {return roundDefinition;}
- (CourseDO*)course {return parentCourse;}

@end
