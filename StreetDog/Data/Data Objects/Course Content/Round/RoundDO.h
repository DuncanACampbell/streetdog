//
//  RoundDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoundDefinitionDO.h"

@class CourseDO;

@interface RoundDO : NSObject {
    
    RoundDefinitionDO *roundDefinition;
    CourseDO *parentCourse;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (RoundDefinitionDO*)roundDefinition;
- (CourseDO*)course;

@end
