//
//  SceneDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SceneDO.h"
#import "CourseDO.h"

@implementation SceneDO

static NSString* const kSceneIndex = @"sceneIndex";
static NSString* const kSceneType = @"sceneType";
static NSString* const kImageName = @"image";
static NSString* const kPlayAnswerAudio = @"playAnswerAudio";
static NSString* const kSlideshowImages = @"slideshowImages";
static NSString* const kHelpText = @"helpText";
static NSString* const kQuestionPhraseInstruction = @"questionPhraseInstruction";
static NSString* const kAnswerPhraseInstruction = @"answerPhraseInstruction";
static NSString* const kWrongAnswerPhraseInstructions = @"wrongAnswerPhraseInstructions";
static NSString* const kWrongAnswerLessons = @"wrongAnswerLessons";
static NSString* const kParentCourse = @"parentCourse";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        sceneIndex = 0;
        sceneType = kSceneTypeNotDefined;
        imageName = @"";
        playAnswerAudio = false;
        slideshowImages = [NSMutableArray array];
        helpText = @"";
        questionPhraseInstruction = nil;
        answerPhraseInstruction = nil;
        wrongAnswerPhraseInstructions = [NSMutableArray array];
        wrongAnswerLessons = [NSMutableArray array];
        parentCourse = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        sceneIndex = [[decoder decodeObjectForKey:kSceneIndex] intValue];
        sceneType = [[decoder decodeObjectForKey:kSceneType] intValue];
        imageName = [decoder decodeObjectForKey:kImageName];
        playAnswerAudio = [[decoder decodeObjectForKey:kPlayAnswerAudio] boolValue];
        slideshowImages = [decoder decodeObjectForKey:kSlideshowImages];
        helpText = [decoder decodeObjectForKey:kHelpText];
        questionPhraseInstruction = [decoder decodeObjectForKey:kQuestionPhraseInstruction];
        answerPhraseInstruction = [decoder decodeObjectForKey:kAnswerPhraseInstruction];
        wrongAnswerPhraseInstructions = [decoder decodeObjectForKey:kWrongAnswerPhraseInstructions];
        wrongAnswerLessons = [decoder decodeObjectForKey:kWrongAnswerLessons];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:[NSNumber numberWithInt:sceneIndex] forKey:kSceneIndex];
    [encoder encodeObject:[NSNumber numberWithInt:sceneType] forKey:kSceneType];
    [encoder encodeObject:imageName forKey:kImageName];
    [encoder encodeObject:[NSNumber numberWithInt:playAnswerAudio] forKey:kPlayAnswerAudio];
    [encoder encodeObject:slideshowImages forKey:kSlideshowImages];
    [encoder encodeObject:helpText forKey:kHelpText];
    [encoder encodeObject:questionPhraseInstruction forKey:kQuestionPhraseInstruction];
    [encoder encodeObject:answerPhraseInstruction forKey:kAnswerPhraseInstruction];
    [encoder encodeObject:wrongAnswerPhraseInstructions forKey:kWrongAnswerPhraseInstructions];
    [encoder encodeObject:wrongAnswerLessons forKey:kWrongAnswerLessons];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict  withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    parentCourse = course;
    
    sceneIndex = [[sourceDict objectForKey:kSceneIndex] intValue];
    helpText = [sourceDict objectForKey:kHelpText];
    playAnswerAudio = [[sourceDict objectForKey:kPlayAnswerAudio] boolValue];
    
    imageName = [sourceDict objectForKey:kImageName];
    slideshowImages = [NSMutableArray arrayWithArray:[sourceDict objectForKey:kSlideshowImages]];
    
    NSString *theSceneType = [sourceDict objectForKey:kSceneType];
    if ([theSceneType isEqualToString:@"stSlideshow"]) {
        sceneType = kSceneTypeSlideShow;
    } else if ([theSceneType isEqualToString:@"stTitle"]) {
        sceneType = kSceneTypeTitle;
    } else if ([theSceneType isEqualToString:@"stAudio"]) {
        sceneType = kSceneTypeAudio;
    } else if ([theSceneType isEqualToString:@"stAction"]) {
        sceneType = kSceneTypeAction;
    } else if ([theSceneType isEqualToString:@"stAudioAction"]) {
        sceneType = kSceneTypeAudioAction;
    }
    
    NSDictionary *questionDict = [sourceDict objectForKey:kQuestionPhraseInstruction];
    if (questionDict) {
        questionPhraseInstruction = [[PhraseInstructionDO alloc] initFromDictionary:questionDict withCourse:course];
    }
    
    NSDictionary *answerDict = [sourceDict objectForKey:kAnswerPhraseInstruction];
    if (answerDict) {
        answerPhraseInstruction = [[PhraseInstructionDO alloc] initFromDictionary:answerDict withCourse:course];
    }
    
    wrongAnswerPhraseInstructions = [NSMutableArray array];
    for (NSDictionary *wrongAnswerDict in [sourceDict objectForKey:kWrongAnswerPhraseInstructions]) {
        PhraseInstructionDO *wrongAnswerPI = [[PhraseInstructionDO alloc] initFromDictionary:wrongAnswerDict withCourse:course];
        [wrongAnswerPhraseInstructions addObject:wrongAnswerPI];
    }
    
    wrongAnswerLessons = [NSMutableArray array];
    for (NSString *wrongAnswerLessonID in [sourceDict objectForKey:@"lessons"]) {
        LessonDO *thisLessonDO = [parentCourse lessonWithID:wrongAnswerLessonID];
        [wrongAnswerLessons addObject:thisLessonDO];
    }
}

#pragma mark - Getters
- (int)sceneIndex {return sceneIndex;}
- (SceneType)sceneType {return sceneType;}
- (NSString*)imageName {return imageName;}
- (NSArray*)slideshowImages {return [NSArray arrayWithArray:slideshowImages];}
- (NSString*)helpText {return helpText;}
- (BOOL)playAnswerAudio {return playAnswerAudio;}
- (PhraseInstructionDO*)questionPhraseInstruction {return questionPhraseInstruction;}
- (PhraseInstructionDO*)answerPhraseInstruction {return answerPhraseInstruction;}
- (NSArray*)wrongAnswerPhraseInstructions {return [NSArray arrayWithArray:wrongAnswerPhraseInstructions];}
- (NSArray*)wrongAnswerLessons {return [NSArray arrayWithArray:wrongAnswerLessons];}
- (CourseDO*)parentCourse {return parentCourse;}

@end
