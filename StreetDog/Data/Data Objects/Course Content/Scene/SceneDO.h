//
//  SceneDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CourseDO;
@class PhraseInstructionDO;

typedef enum {
    kSceneTypeNotDefined,
    kSceneTypeTitle,
    kSceneTypeSlideShow,
    kSceneTypeAudio,
    kSceneTypeAction,
    kSceneTypeAudioAction
} SceneType;

@interface SceneDO : NSObject {
    
    int sceneIndex;
    SceneType sceneType;

    NSString *imageName;
    NSMutableArray *slideshowImages;
    
    NSString *helpText;
    BOOL playAnswerAudio;
    
    PhraseInstructionDO *questionPhraseInstruction;
    PhraseInstructionDO *answerPhraseInstruction;
    NSMutableArray *wrongAnswerPhraseInstructions;
    
    NSMutableArray *wrongAnswerLessons;
    
    CourseDO *parentCourse;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (int)sceneIndex;
- (SceneType)sceneType;
- (NSString*)imageName;
- (NSArray*)slideshowImages;
- (NSString*)helpText;
- (BOOL)playAnswerAudio;
- (PhraseInstructionDO*)questionPhraseInstruction;
- (PhraseInstructionDO*)answerPhraseInstruction;
- (NSArray*)wrongAnswerPhraseInstructions;
- (NSArray*)wrongAnswerLessons;
- (CourseDO*)parentCourse;

@end
