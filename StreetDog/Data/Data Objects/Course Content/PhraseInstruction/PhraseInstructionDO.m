//
//  PhraseInstructionDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "PhraseInstructionDO.h"
#import "CatalogueController.h"
#import "VariableVO.h"

@implementation PhraseInstructionDO

static NSString* const kConceptInstruction = @"conceptInstruction";
static NSString* const kParameterValues = @"parameterValues";
static NSString* const kFilterGroups = @"filterGroups";
static NSString* const kVoice = @"voice";
static NSString* const kSourceVoice = @"sourceVoice";
static NSString* const kDestinationVoice = @"destinationVoice";
static NSString* const kParentCourse = @"parentCourse";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        conceptInstruction = nil;
        parameterValues = [NSMutableDictionary dictionary];
        filterGroups = [NSMutableDictionary dictionary];
        sourceVoice = nil;
        destinationVoice = nil;
        parentCourse = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        conceptInstruction = [decoder decodeObjectForKey:kConceptInstruction];
        parameterValues = [decoder decodeObjectForKey:kParameterValues];
        filterGroups = [decoder decodeObjectForKey:kFilterGroups];
        sourceVoice = [decoder decodeObjectForKey:kSourceVoice];
        destinationVoice = [decoder decodeObjectForKey:kDestinationVoice];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:conceptInstruction forKey:kConceptInstruction];
    [encoder encodeObject:parameterValues forKey:kParameterValues];
    [encoder encodeObject:filterGroups forKey:kFilterGroups];
    [encoder encodeObject:sourceVoice forKey:kSourceVoice];
    [encoder encodeObject:destinationVoice forKey:kDestinationVoice];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
}

- (NSString*)description {return conceptInstruction.identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    parentCourse = course;
    
    NSString *conceptInstructionID = [sourceDict objectForKey:@"conceptInstructionID"];
    conceptInstruction = [DataController.sharedHelper.catalogue conceptInstructionWithID:conceptInstructionID];
    
    //set any parameters or filter groups (variables will be set at run-time)
    parameterValues = [NSMutableDictionary dictionary];
    NSDictionary *parameters = [sourceDict objectForKey:@"parameters"];
    for (NSString *parameterGroupID in [parameters allKeys]) {
        NSString *phraseID = [parameters objectForKey:parameterGroupID];
        PhraseDO *phrase = [DataController.sharedHelper.catalogue phraseWithID:phraseID];
        [parameterValues setObject:phrase forKey:parameterGroupID];
    }
    
    filterGroups = [NSMutableDictionary dictionary];
    NSDictionary *thefilterGroups = [sourceDict objectForKey:@"filterGroups"];
    for (NSString *parentFilterGroupID in [thefilterGroups allKeys]) {
        NSString *childFilterGroupID = [thefilterGroups objectForKey:parentFilterGroupID];
        ParameterGroupDO *childFilterGroup = [DataController.sharedHelper.catalogue parameterGroupWithID:childFilterGroupID];
        [filterGroups setObject:childFilterGroup forKey:parentFilterGroupID];
    }
    
    variableMappings = [NSMutableDictionary dictionary];
    NSDictionary *theVariableMappings = [sourceDict objectForKey:@"variableMappings"];
    for (NSString *parameterGroupID in [theVariableMappings allKeys]) {
        NSString *variableID = [theVariableMappings objectForKey:parameterGroupID];
        [variableMappings setObject:parameterGroupID forKey:variableID];
    }
    
    NSString *voiceID = [sourceDict objectForKey:kVoice];
    if (voiceID) {
        
        if ([voiceID isEqualToString:@"vUser"]) {
            BOOL isCurrentUserMale = (DataController.sharedHelper.users.currentUser.gender == kGenderMale);
            sourceVoice = [DataController.sharedHelper.catalogue userVoiceWithLanguage:course.destinationLanguage isMale:isCurrentUserMale];
        } else {
            sourceVoice = [DataController.sharedHelper.catalogue voiceWithID:voiceID];
        }
        destinationVoice = nil;
    } else {
        sourceVoice = [DataController.sharedHelper.catalogue tutorVoiceWithLanguage:course.sourceLanguage];
        destinationVoice = [DataController.sharedHelper.catalogue tutorVoiceWithLanguage:course.destinationLanguage];
    }
}

#pragma mark - Getters
- (ConceptInstructionDO*)conceptInstruction {return conceptInstruction;}
- (VoiceDO*)sourceVoice {return sourceVoice;}
- (VoiceDO*)destinationVoice {return destinationVoice;}
- (NSDictionary*)parameterValues {return [NSDictionary dictionaryWithDictionary:parameterValues];}
- (NSDictionary*)filterGroups {return [NSDictionary dictionaryWithDictionary:filterGroups];}

#pragma mark - Permutations
- (NSArray*)getConceptPermutationsWithVariables:(NSArray*)variables {
    
    //if any variableMappings exist, substitute them into the parameter set
    NSMutableDictionary *theseParameters = [parameterValues mutableCopy];
    for (NSString *variableID in [variableMappings allKeys]) {
        for (VariableVO *thisVariable in variables) {
            if ([thisVariable.identifier isEqualToString:variableID]) {
                
                NSString *parameterGroupID = [variableMappings objectForKey:variableID];
                [theseParameters setObject:thisVariable.value forKey:parameterGroupID];
                break;
            }
        }
    }
    
    //now loop through the conceptInstruction's permutations and collect any which meet the criteria
    NSMutableArray *matchingPermutations = [NSMutableArray array];
    for (ConceptPermutationDO *permutation in conceptInstruction.allPermutations) {
        
        bool isPermutationValid = true;
        
        //do all the parameters on this object match this permutation
        for (NSString *permutationParameterGroupID in [permutation.parameterValues allKeys]) {
            PhraseDO *permutationParameterValue = [permutation.parameterValues objectForKey:permutationParameterGroupID];
            
            //look the matching parameterGroupID on the permutation
            PhraseDO *inputParameterValue = [theseParameters objectForKey:permutationParameterGroupID];
            ParameterGroupDO *filterGroup = [filterGroups objectForKey:permutationParameterGroupID];
            
            if (filterGroup) {
                //permutation validaity will be assessed below
            } else if (inputParameterValue) {
                //if these don't match, move onto the next
                if (inputParameterValue != permutationParameterValue) {
                    isPermutationValid = false;
                    break;
                }
            } else {
                NSLog(@"No input parameter found for key: %@", permutationParameterGroupID);
                isPermutationValid = false;
                break;
            }
        }
        
        //only continue if the permutation is still valid
        if (isPermutationValid) {
            
            //now check that the permutation's phrases fall inside the filterGroups
            for (NSString *parameterGroupID in filterGroups.allKeys) {
                ParameterGroupDO *childParameterGroup = [filterGroups objectForKey:parameterGroupID];
                
                PhraseDO *permutationParameterValue = [permutation.parameterValues objectForKey:parameterGroupID];
                
                //does this Phrase exist inside the childParameterGroup
                bool isPhraseMatch = false;
                for (PhraseDO *phraseInGroup in childParameterGroup.phrases) {
                    if (permutationParameterValue == phraseInGroup) {
                        isPhraseMatch = true;
                        break;
                    }
                }
                
                if (!isPhraseMatch) {
                    isPermutationValid = false;
                    break;
                }
            }
            
            //if the permutation is still valid, this is a valid value to return
            if (isPermutationValid) {
                [matchingPermutations addObject:permutation];
            }
        }
    }
    
    return [NSArray arrayWithArray:matchingPermutations];
}

#pragma mark - TranslationPairs
- (NSArray*)translationPairsWithVariables:(NSArray*)variables {
    
    //a variable is essentially another parameterValue (just at run-time) so create a new "parameters" collection
    NSArray *matchingPermutations = [self getConceptPermutationsWithVariables:variables];
    
    //return TranslationPairs for the relevant languages
    NSMutableArray *translationPairs = [NSMutableArray array];
    for (ConceptPermutationDO *thisPermutation in matchingPermutations) {
        TranslationPairDO *translationPair = [[TranslationPairDO alloc] initFromConceptPermutation:thisPermutation withPhraseInstruction:self];
        [translationPairs addObject:translationPair];
    }
    
    return [NSArray arrayWithArray:translationPairs];
}

- (TranslationPairDO*)translationPairWithVariables:(NSArray*)variables {
    
    NSArray *translationPairs = [self translationPairsWithVariables:variables];
    if (translationPairs.count == 0) {
        return nil;
    } else {
        return [translationPairs objectAtIndex:0];
    }
}

#pragma mark - TranslationPairs
- (NSArray*)audioPhrasesWithVariables:(NSArray*)variables {
    
    //a variable is essentially another parameterValue (just at run-time) so create a new "parameters" collection
    NSArray *matchingPermutations = [self getConceptPermutationsWithVariables:variables];
    
    //return TranslationPairs for the relevant languages
    NSMutableArray *audioPhrases = [NSMutableArray array];
    for (ConceptPermutationDO *thisPermutation in matchingPermutations) {
        AudioPhraseDO *audioPhrase = [[AudioPhraseDO alloc] initFromConceptPermutation:thisPermutation withVoice:sourceVoice];
        [audioPhrases addObject:audioPhrase];
    }
    
    return [NSArray arrayWithArray:audioPhrases];
}

- (AudioPhraseDO*)audioPhraseWithVariables:(NSArray*)variables {
    
    NSArray *audioPhrases = [self audioPhrasesWithVariables:variables];
    if (audioPhrases.count == 0) {
        return nil;
    } else {
        return [audioPhrases objectAtIndex:0];
    }
}

@end
