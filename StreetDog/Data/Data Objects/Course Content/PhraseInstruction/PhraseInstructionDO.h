//
//  PhraseInstructionDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConceptInstructionDO.h"
#import "VoiceDO.h"

@class CourseDO;

@interface PhraseInstructionDO : NSObject {
    
    ConceptInstructionDO *conceptInstruction;
    NSMutableDictionary *parameterValues;
    NSMutableDictionary *filterGroups;
    NSMutableDictionary *variableMappings;
    
    VoiceDO *sourceVoice;
    VoiceDO *destinationVoice;
    
    CourseDO *parentCourse;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (ConceptInstructionDO*)conceptInstruction;
- (VoiceDO*)sourceVoice;
- (VoiceDO*)destinationVoice;
- (NSDictionary*)parameterValues;
- (NSDictionary*)filterGroups;

- (TranslationPairDO*)translationPairWithVariables:(NSArray*)variables;
- (NSArray*)translationPairsWithVariables:(NSArray*)variables;

- (NSArray*)audioPhrasesWithVariables:(NSArray*)variables;
- (AudioPhraseDO*)audioPhraseWithVariables:(NSArray*)variables;

@end
