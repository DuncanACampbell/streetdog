//
//  SlideDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CourseDO;

@interface SlideDO : NSObject {
    
    SlideType slideType;
    NSMutableArray *phraseInstructions;
    NSString *helpText;
    
    CourseDO *parentCourse;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (SlideType)slideType;
- (NSArray*)translationPairs;
- (NSString*)helpText;
- (CourseDO*)course;

@end
