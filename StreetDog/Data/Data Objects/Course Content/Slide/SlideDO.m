//
//  SlideDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "SlideDO.h"
#import "CourseDO.h"

@implementation SlideDO

static NSString* const kSlideType = @"slideType";
static NSString* const kPhraseInstructions = @"phraseInstructions";
static NSString* const kTranslationPairs = @"translationPairs";
static NSString* const kHelpText = @"helpText";
static NSString* const kParentCourse = @"parentCourse";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        slideType = kSlideTypeSinglePhraseTranslation;
        phraseInstructions = [NSMutableArray array];
        helpText = @"";
        parentCourse = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        slideType = [[decoder decodeObjectForKey:kSlideType] intValue];
        phraseInstructions = [decoder decodeObjectForKey:kPhraseInstructions];
        helpText = [decoder decodeObjectForKey:kHelpText];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:[NSNumber numberWithInt:slideType] forKey:kSlideType];
    [encoder encodeObject:phraseInstructions forKey:kPhraseInstructions];
    [encoder encodeObject:helpText forKey:kHelpText];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict  withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    parentCourse = course;
    
    NSString *theSlideType = [sourceDict objectForKey:kSlideType];
    if ([theSlideType isEqualToString:@"SinglePhraseTranslation"]) {
        slideType = kSlideTypeSinglePhraseTranslation;
    } else if ([theSlideType isEqualToString:@"NumericSequence"]) {
        slideType = kSlideTypeNumericList;
    }

    phraseInstructions = [NSMutableArray array];
    for (NSDictionary *phraseInstructionDict in [sourceDict objectForKey:kPhraseInstructions]) {
        PhraseInstructionDO *phraseInstruction = [[PhraseInstructionDO alloc] initFromDictionary:phraseInstructionDict withCourse:course];
        [phraseInstructions addObject:phraseInstruction];
    }
    
    helpText = [sourceDict objectForKey:kHelpText];
}

#pragma mark - Getters
- (SlideType)slideType {return slideType;}
- (NSString*)helpText {return helpText;}
- (CourseDO*)course {return parentCourse;}

- (NSArray*)translationPairs {
    
    NSMutableArray *translationPairs = [NSMutableArray array];
    for (PhraseInstructionDO *thisPhraseInstruction in phraseInstructions) {
        NSArray *theseTranslationPairs = [thisPhraseInstruction translationPairsWithVariables:nil];
        [translationPairs addObjectsFromArray:theseTranslationPairs];
    }
    return translationPairs;
}

@end
