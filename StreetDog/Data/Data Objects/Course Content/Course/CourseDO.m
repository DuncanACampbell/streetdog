//
//  CourseDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "CourseDO.h"

@implementation CourseDO

static NSString* const kIdentifier = @"courseID";
static NSString* const kTitle = @"title";

static NSString* const kSourceLanguage = @"sourceLanguage";
static NSString* const kDestinationLanguage = @"destinationLanguage";
static NSString* const kLessons = @"lessons";
static NSString* const kConversations = @"conversations";

static NSString* const kSourceLanguageID = @"sourceLanguageID";
static NSString* const kDestinationLanguageID = @"destinationLanguageID";
static NSString* const kLessonID = @"lessonID";
static NSString* const kConversationID = @"conversationID";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
        
        sourceLanguage = nil;
        destinationLanguage = nil;
        
        lessons = [NSMutableDictionary dictionary];
        conversations = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        sourceLanguage = [decoder decodeObjectForKey:kSourceLanguage];
        destinationLanguage = [decoder decodeObjectForKey:kDestinationLanguage];
        lessons = [decoder decodeObjectForKey:kLessons];
        conversations = [decoder decodeObjectForKey:kConversations];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:sourceLanguage forKey:kSourceLanguage];
    [encoder encodeObject:destinationLanguage forKey:kDestinationLanguage];
    [encoder encodeObject:lessons forKey:kLessons];
    [encoder encodeObject:conversations forKey:kConversations];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict {
    title = [sourceDict objectForKey:kTitle];
    
    //set the source and destination languages
    NSString *sourceLanguageID = [sourceDict objectForKey:kSourceLanguageID];
    sourceLanguage = [DataController.sharedHelper.catalogue languageWithID:sourceLanguageID];
    NSString *destinationLanguageID = [sourceDict objectForKey:kDestinationLanguageID];
    destinationLanguage = [DataController.sharedHelper.catalogue languageWithID:destinationLanguageID];
    
    //lessons
    NSMutableDictionary *newLessons = [NSMutableDictionary dictionary];
    for (NSDictionary *lessonDict in [[sourceDict objectForKey:kLessons] allValues]) {
        NSString *lessonID = [lessonDict objectForKey:kLessonID];
        LessonDO *thisLesson = [self lessonExistsWithID:lessonID];
        if (thisLesson) {
            [thisLesson updateFromDictionary:lessonDict withCourse:self];
        } else {
            thisLesson = [[LessonDO alloc] initFromDictionary:lessonDict withCourse:self];
        }
        [newLessons setObject:thisLesson forKey:lessonID];
    }
    lessons = newLessons;
    
    //conversations
    NSMutableDictionary *newConversations = [NSMutableDictionary dictionary];
    for (NSDictionary *conversationDict in [[sourceDict objectForKey:kConversations] allValues]) {
        NSString *conversationID = [conversationDict objectForKey:kConversationID];
        ConversationDO *thisConversation = [self conversationExistsWithID:conversationID];
        if (thisConversation) {
            [thisConversation updateFromDictionary:conversationDict withCourse:self];
        } else {
            thisConversation = [[ConversationDO alloc] initFromDictionary:conversationDict withCourse:self];
        }
        [newConversations setObject:thisConversation forKey:conversationID];
    }
    conversations = newConversations;
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (LanguageDO*)sourceLanguage {return sourceLanguage;}
- (LanguageDO*)destinationLanguage {return destinationLanguage;}

#pragma mark - Getters (exists)
- (LessonDO*)lessonExistsWithID:(NSString*)lessonID {
    return [lessons objectForKey:lessonID];
}

- (ConversationDO*)conversationExistsWithID:(NSString*)conversationID {
    return [conversations objectForKey:conversationID];
}

#pragma mark - Getters (external)
- (LessonDO*)lessonWithID:(NSString*)lessonID {
    LessonDO *returnValue = [lessons objectForKey:lessonID];
    if (!returnValue) [self objectNotFound:lessonID];
    return returnValue;
}

- (ConversationDO*)conversationWithID:(NSString*)conversationID {
    ConversationDO *returnValue = [conversations objectForKey:conversationID];
    if (!returnValue) [self objectNotFound:conversationID];
    return returnValue;
}

- (ConversationDO*)firstConversation {
    return [[conversations allValues] objectAtIndex:0];
}

#pragma mark - errors
- (void)objectNotFound:(NSString*)objectID {
    NSLog(@"Object not found: %@", objectID);
}
@end
