//
//  CourseDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoiceDO.h"
#import "PhraseDO.h"
#import "ConceptInstructionDO.h"
#import "LessonDO.h"
#import "RoundDO.h"
#import "ParameterGroupDO.h"
#import "LanguageDO.h"
#import "ConversationDO.h"

@interface CourseDO : NSObject {
    
    NSString *identifier;
    NSString *title;
    
    LanguageDO *sourceLanguage;
    LanguageDO *destinationLanguage;
    
    NSMutableDictionary *lessons;
    NSMutableDictionary *conversations;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict;
- (void)updateFromDictionary:(NSDictionary*)sourceDict;

- (NSString*)identifier;
- (LanguageDO*)sourceLanguage;
- (LanguageDO*)destinationLanguage;

- (LessonDO*)lessonWithID:(NSString*)lessonID;
- (ConversationDO*)conversationWithID:(NSString*)conversationID;
- (ConversationDO*)firstConversation;

@end
