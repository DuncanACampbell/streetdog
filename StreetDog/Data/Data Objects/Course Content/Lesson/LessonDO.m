//
//  LessonDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "LessonDO.h"
#import "CourseDO.h"

@implementation LessonDO

static NSString* const kIdentifier = @"lessonID";
static NSString* const kTitle = @"title";
static NSString* const kSubtitle = @"subtitle";
static NSString* const kParentCourse = @"parentCourse";
static NSString* const kPhraseInstructionGroups = @"phraseInstructionGroups";
static NSString* const kRounds = @"rounds";
static NSString* const kSlides = @"slides";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        
        title = @"";
        subtitle = @"";
        
        parentCourse = nil;
        
        phraseInstructionGroups = [NSMutableArray array];
        rounds = [NSMutableArray array];
        slides = [NSMutableArray array];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        subtitle = [decoder decodeObjectForKey:kSubtitle];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
        phraseInstructionGroups = [decoder decodeObjectForKey:kPhraseInstructionGroups];
        rounds = [decoder decodeObjectForKey:kRounds];
        slides = [decoder decodeObjectForKey:kSlides];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:subtitle forKey:kSubtitle];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
    [encoder encodeObject:phraseInstructionGroups forKey:kPhraseInstructionGroups];
    [encoder encodeObject:rounds forKey:kRounds];
    [encoder encodeObject:slides forKey:kSlides];
}

- (NSString*)description {return identifier;}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course{
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    title = [sourceDict objectForKey:kTitle];
    subtitle = [sourceDict objectForKey:kSubtitle];
   
    parentCourse = course;

    phraseInstructionGroups = [NSMutableArray array];
    for (NSArray *thePhraseInstructionGroups in [sourceDict objectForKey:@"phraseInstructionGroups"]) {
        
        NSMutableArray *thisPhraseInstructionGroup = [NSMutableArray array];
        for (NSDictionary *phraseInstructionDict in thePhraseInstructionGroups) {
            
            PhraseInstructionDO *phraseInstruction = [[PhraseInstructionDO alloc] initFromDictionary:phraseInstructionDict withCourse:course];
            [thisPhraseInstructionGroup addObject:phraseInstruction];
        }
        [phraseInstructionGroups addObject:thisPhraseInstructionGroup];
    }
    
    rounds = [NSMutableArray array];
    for (NSDictionary *roundDict in [sourceDict objectForKey:kRounds]) {
        RoundDO *thisRound = [[RoundDO alloc] initFromDictionary:roundDict withCourse:course];
        [rounds addObject:thisRound];
    }
    
    slides = [NSMutableArray array];
    for (NSDictionary *slideDict in [sourceDict objectForKey:kSlides]) {
        SlideDO *thisSlide = [[SlideDO alloc] initFromDictionary:slideDict withCourse:course];
        [slides addObject:thisSlide];
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}
- (CourseDO*)course {return parentCourse;}
- (NSArray*)rounds {return [NSArray arrayWithArray:rounds];}
- (NSArray*)slides {return [NSArray arrayWithArray:slides];}

- (NSArray*)translationPairGroups {
    
    NSMutableArray *translationPairGroups = [NSMutableArray array];
    for (NSArray *phraseInstructionGroupArray in phraseInstructionGroups) {

        NSMutableArray *thisTranslationPairGroup = [NSMutableArray array];
        for (PhraseInstructionDO *phraseInstruction in phraseInstructionGroupArray) {

            NSArray *theseTranslationPairs = [phraseInstruction translationPairsWithVariables:nil];
            for (TranslationPairDO *translationPair in theseTranslationPairs) {
                [thisTranslationPairGroup addObject:translationPair];
            }
        }
        [translationPairGroups addObject:thisTranslationPairGroup];
    }
    return translationPairGroups;
}
@end
