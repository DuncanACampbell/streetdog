//
//  LessonDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlideDO.h"
#import "RoundDO.h"
#import "PhraseInstructionDO.h"

@class CourseDO;

@interface LessonDO : NSObject {
    
    NSString *identifier;
    
    NSString *title;
    NSString *subtitle;
    
    CourseDO *parentCourse;
    
    NSMutableArray *phraseInstructionGroups;
    NSMutableArray *rounds;
    NSMutableArray *slides;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (NSString*)identifier;
- (NSString*)title;
- (NSString*)subtitle;
- (CourseDO*)course;
- (NSArray*)translationPairGroups;
- (NSArray*)rounds;
- (NSArray*)slides;

@end
