//
//  ConversationDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SceneDO.h"
#import "VariableDefinitionDO.h"

@class CourseDO;

@interface ConversationDO : NSObject {
    
    NSString *identifier;
    NSString *title;
    NSString *subtitle;
    NSMutableArray *variables;
    NSMutableArray *scenes;
    
    CourseDO *parentCourse;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (NSString*)identifier;
- (NSString*)title;
- (NSString*)subtitle;
- (NSArray*)variables;
- (NSArray*)scenes;

@end
