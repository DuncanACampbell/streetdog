//
//  ConversationDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "ConversationDO.h"
#import "CourseDO.h"

@implementation ConversationDO

static NSString* const kIdentifier = @"conversationID";
static NSString* const kTitle = @"title";
static NSString* const kSubtitle = @"subtitle";
static NSString* const kVariables = @"variables";
static NSString* const kScenes = @"scenes";
static NSString* const kParentCourse = @"parentCourse";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        title = @"";
        subtitle = @"";
        variables = [NSMutableArray array];
        scenes = [NSMutableArray array];
        parentCourse = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        title = [decoder decodeObjectForKey:kTitle];
        subtitle = [decoder decodeObjectForKey:kSubtitle];
        variables = [decoder decodeObjectForKey:kVariables];
        scenes = [decoder decodeObjectForKey:kScenes];
        parentCourse = [decoder decodeObjectForKey:kParentCourse];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:title forKey:kTitle];
    [encoder encodeObject:subtitle forKey:kSubtitle];
    [encoder encodeObject:variables forKey:kVariables];
    [encoder encodeObject:scenes forKey:kScenes];
    [encoder encodeObject:parentCourse forKey:kParentCourse];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        identifier = [sourceDict objectForKey:kIdentifier];
        [self updateFromDictionary:sourceDict  withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    parentCourse = course;
    
    title = [sourceDict objectForKey:kTitle];
    subtitle = [sourceDict objectForKey:kSubtitle];
    
    //variables
    variables = [NSMutableArray array];
    for (NSDictionary *variableDict in [sourceDict objectForKey:kVariables]) {
        VariableDefinitionDO *variable = [[VariableDefinitionDO alloc] initFromDictionary:variableDict withCourse:course];
        [variables addObject:variable];
    }
    
    //scenes
    scenes = [NSMutableArray array];
    for (NSDictionary *sceneDict in [sourceDict objectForKey:kScenes]) {
        SceneDO *thisScene = [[SceneDO alloc] initFromDictionary:sceneDict withCourse:course];
        [scenes addObject:thisScene];
    }
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (NSString*)title {return title;}
- (NSString*)subtitle {return subtitle;}
- (NSArray*)variables {return [NSArray arrayWithArray:variables];}
- (NSArray*)scenes {return [NSArray arrayWithArray:scenes];}

@end
