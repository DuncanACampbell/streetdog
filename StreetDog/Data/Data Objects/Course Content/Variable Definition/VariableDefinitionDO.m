//
//  VariableDefinitionDO.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "VariableDefinitionDO.h"
#import "CourseDO.h"

@implementation VariableDefinitionDO

static NSString* const kIdentifier = @"variableID";
static NSString* const kParameterGroup = @"parameterGroup";
static NSString* const kParameterGroupID = @"parameterGroupID";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        identifier = @"";
        parameterGroup = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        identifier = [decoder decodeObjectForKey:kIdentifier];
        parameterGroup = [decoder decodeObjectForKey:kParameterGroup];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:identifier forKey:kIdentifier];
    [encoder encodeObject:parameterGroup forKey:kParameterGroup];
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    self = [self init];
    if (self) {
        [self updateFromDictionary:sourceDict withCourse:course];
    }
    return self;
}

- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course {
    
    identifier = [sourceDict objectForKey:kIdentifier];

    NSString *parameterGroupID = [sourceDict objectForKey:kParameterGroupID];
    parameterGroup = [DataController.sharedHelper.catalogue parameterGroupWithID:parameterGroupID];
}

#pragma mark - Getters
- (NSString*)identifier {return identifier;}
- (ParameterGroupDO*)parameterGroup {return parameterGroup;}

- (PhraseDO*)randomValue {    
    int randomIndex = arc4random() % parameterGroup.phrases.count;
    return [parameterGroup.phrases objectAtIndex:randomIndex];
}

@end
