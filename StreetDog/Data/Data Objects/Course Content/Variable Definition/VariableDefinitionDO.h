//
//  VariableDefinitionDO.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/26/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CourseDO;
@class ParameterGroupDO;
@class PhraseDO;

@interface VariableDefinitionDO : NSObject {
    
    NSString *identifier;
    ParameterGroupDO *parameterGroup;
}

- (id)initFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;
- (void)updateFromDictionary:(NSDictionary*)sourceDict withCourse:(CourseDO*)course;

- (NSString*)identifier;
- (ParameterGroupDO*)parameterGroup;
- (PhraseDO*)randomValue;

@end
