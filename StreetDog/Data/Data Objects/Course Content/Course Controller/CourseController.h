//
//  CourseController.h
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CourseDO.h"

@interface CourseController : NSObject {
    
    NSMutableDictionary *courses;
}

- (void)updateWithArray:(NSArray*)courseArray;

- (NSArray*)courses;
- (CourseDO*)courseWithID:(NSString*)courseID;

@end
