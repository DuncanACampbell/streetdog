//
//  CourseController.m
//  StreetDog
//
//  Created by Duncan Campbell on 10/25/12.
//  Copyright (c) 2012 Duncan A. Campbell. All rights reserved.
//

#import "CourseController.h"

@implementation CourseController

static NSString* const kCourses = @"courses";
static NSString* const kCourseID = @"courseID";

#pragma mark - Initialisation
- (id)init
{
    self = [super init];
    if (self) {
        courses = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        courses = [decoder decodeObjectForKey:kCourses];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:courses forKey:kCourses];
}

- (void)updateWithArray:(NSArray*)courseArray {
    
    //add, amend or delete any courses based on the array passed in
    NSMutableDictionary *newCourses = [NSMutableDictionary dictionary];
    for (NSDictionary *courseDict in courseArray) {
        NSString *courseID = [courseDict objectForKey:@"courseID"];
        CourseDO *thisCourse = [self courseWithID:courseID];
        if (thisCourse) {
            [thisCourse updateFromDictionary:courseDict];
        } else {
            thisCourse = [[CourseDO alloc] initFromDictionary:courseDict];
        }
        [newCourses setObject:thisCourse forKey:courseID];
    }
    courses = newCourses;
}

#pragma mark - Getters
- (NSArray*)courses {
    return [courses allValues];
}

- (CourseDO*)courseWithID:(NSString*)courseID {
    return [courses objectForKey:courseID];
}
@end
